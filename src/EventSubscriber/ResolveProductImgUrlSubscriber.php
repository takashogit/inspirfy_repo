<?php
namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use App\Entity\ProductImg;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Vich\UploaderBundle\Storage\StorageInterface;

final class ResolveProductImgUrlSubscriber
implements EventSubscriberInterface
{
    private StorageInterface $storage;
    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                'onPreSerialize',
                EventPriorities::PRE_SERIALIZE
            ],
        ];
    }

    public function onPreSerialize(ViewEvent $event): void
    {
        $controllerResult = $event->getControllerResult();
        $request = $event->getRequest();

        if($controllerResult instanceof Response
            ||
        !$request->attributes->getBoolean('_api_respond',true))
        {
            return;
        }

        if(!($attributes = RequestAttributesExtractor::extractAttributes($request))
            ||
        !\is_a($attributes['resource_class'],ProductImg::class,true
        )){
            return;
        }
        $productImages = $controllerResult;
        if(!is_iterable($productImages)){
            $productImages = [$productImages];
        }
        foreach ($productImages as $productImg){
            if(!$productImg instanceof ProductImg){
                continue;
            }

            $productImg->contentUrl = $this->storage->resolveUri($productImg,'file');
        }
    }
}