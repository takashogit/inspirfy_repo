<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use App\Controller\ProductImageUploadActionController;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;



/**
 * ProductImg
 *
 * @ORM\Table(name="product_img", uniqueConstraints={@ORM\UniqueConstraint(name="product_img_hash_product_IDX", columns={"hash_product", "hash_image"})})
 * @ORM\Entity
 * @ApiResource(
 *     iri="http://schema.org/MediaObject",
 *     normalizationContext={
 *         "groups"={"product_img_read"}
 *     },
 *     collectionOperations={
 *         "post"={
 *             "controller"=ProductImageUploadActionController::class,
 *             "deserialize"=false,
 *             "validation_groups"={"Default", "product_img_create"},
 *             "openapi_context"={
 *                 "requestBody"={
 *                     "content"={
 *                         "multipart/form-data"={
 *                             "schema"={
 *                                 "type"="object",
 *                                 "properties"={
 *                                     "file"={
 *                                         "type"="string",
 *                                         "format"="binary"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *         "get",
 *     },
 *     itemOperations={
 *         "get"
 *     }
 * )
 *
 * @Vich\Uploadable
 */
class ProductImg
{

    /**
     * @var string
     *
     * @ORM\Column(name="hash_product", type="string", length=191, nullable=false)
     */
    private $hashProduct;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="hash_image", type="string", length=191, nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $hashImage;

    /**
     * @var string
     *
     * @ORM\Column(name="name_image", type="string", length=255, nullable=false)
     * @Groups({"product_img_read","images"})
     */
    private $nameImage;

    /**
     * @var string
     *
     * @ORM\Column(name="link_image", type="string", length=255, nullable=false)
     *
     * @Groups({"images"})
     */
    private $linkImage;

    /**
     * @var Products
     * @ORM\ManyToOne(targetEntity="Products")
     * @ORM\JoinColumn(name="hash_product", referencedColumnName="hash_product")
     */
    private $products;

    /**
     * @var string|null
     *
     * @ApiProperty(iri="http://schema.org/contentUrl")
     * @Groups({"product_img_read"})
     */
    public $contentUrl;

    /**
     * @var File|null
     * @Assert\NotNull(groups={"product_img_create"})
     * @Vich\UploadableField(mapping="product_img", fileNameProperty="filePath")
     *
     */
    public $file;


    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private ?string $filePath;





    /**
     * @return string|null
     */
    public function getHashProduct(): ?string
    {
        return $this->hashProduct;
    }

    /**
     * @param string $hashProduct
     * @return $this
     */
    public function setHashProduct(string $hashProduct): self
    {
        $this->hashProduct = $hashProduct;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHashImage(): ?string
    {
        return $this->hashImage;
    }

    /**
     * @param string $hashImage
     * @return $this
     */
    public function setHashImage(string $hashImage): self
    {
        $this->hashImage = $hashImage;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNameImage(): ?string
    {
        return $this->nameImage;
    }

    /**
     * @param string $nameImage
     * @return $this
     */
    public function setNameImage(string $nameImage): self
    {
        $this->nameImage = $nameImage;

        return $this;
    }

    /**
     * link image
     * @return string|null
     */
    public function getLinkImage(): ?string
    {
        return $this->linkImage;

    }

    /**
     * @param string $linkImage
     * @return $this
     */
    public function setLinkImage(string $linkImage): self
    {
        $this->linkImage = $linkImage;

        return $this;
    }

    /**
     * @return Products|null
     */
    public function getProducts(): ?Products
    {
        return $this->products;
    }

    /**
     * @param Products|null $products
     * @return $this
     */
    public function setProducts(?Products $products): self
    {
        $this->products = $products;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFilePath(): ?string
    {

        return $this->filePath;
    }

    /**
     * @param string|null $filePath
     * @return $this
     */
    public function setFilePath(?string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }






}
