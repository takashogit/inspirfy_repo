<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ProductsDescription
 *
 * @ORM\Table(name="products_description",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="product_description_hash_product_IDX",
 *      columns={"hash_product"})})
 * @ORM\Entity
 * @ApiResource
 */
class ProductsDescription
{
    /**
     * @var string
     *
     * @ORM\Column(name="hash_product", type="string", length=191, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $hashProduct;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     * @Groups({"description"})
     */
    private $description;

    /**
     * @var Products
     *
     * @ORM\OneToOne(targetEntity="Products", inversedBy="description")
     * @ORM\JoinColumn(name="hash_product", referencedColumnName="hash_product")
     */

    private $product;

    public function getHashProduct(): ?string
    {
        return $this->hashProduct;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getProduct(): ?Products
    {
        return $this->product;
    }

    public function setProduct(?Products $product): self
    {
        $this->product = $product;

        return $this;
    }


}
