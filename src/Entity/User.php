<?php

    namespace App\Entity;

    use ApiPlatform\Core\Annotation\ApiProperty;
    use Doctrine\Common\Collections\ArrayCollection;
    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
    use Symfony\Component\Security\Core\User\UserInterface;
    use ApiPlatform\Core\Annotation\ApiResource;
    use Symfony\Component\Serializer\Annotation\Groups;
    use Symfony\Component\Validator\Constraints as Assert;
;

    /**
     *
     * @ApiResource(iri="http://schema.org/Person")
     * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
     * @UniqueEntity(fields="email", message="Email already taken")*
     * @ApiResource(
     *     attributes={"security"="is_granted('ROLE_ADMIN')"},
     *     normalizationContext={"groups"={"user:output"}},
     * )
     *
     */
    class User implements UserInterface
    {
        const ROLE_COMMENTATOR ="ROLE_COMMENTATOR";
        const ROLE_WRITER = "ROLE_WRITER";
        const ROLE_EDITOR = "ROLE_EDITOR";
        const ROLE_USER = "ROLE_USER";
        const ROLE_ARCHITECT = "ROLE_ARCHITECT";
        const ROLE_SUPERADMIN = "ROLE_SUPERADMIN";
        const DEFAULT_ROLES = [self::ROLE_USER];




        /**
         * @ORM\Id()
         * @ORM\GeneratedValue()
         * @ORM\Column(type="integer")
         * @Groups({"user:output"})
         */
        private $id;

        /**
         * @ORM\Column(type="string", length=180, unique=true)
         * @Assert\Email()
         * @Groups({"user:output"})
         */
        private $email;

        /**
         * @ORM\Column(type="json")
         */
        private $roles = [];

        /**
         * @var string The hashed password
         * @ORM\Column(type="string")
         */
        private $password;

        /**
         * @ApiProperty(iri ="https://schema.org/Person")
         * @ORM\Column(type="string", length=255)
         * @Groups({"user:output"})
         * @ApiProperty()
         */
        private ?string $name;



        /**
         * @ORM\Column(type="string", name="avatar", length=255, nullable=true)
         *
         */
        private ?string $avatar;

        /**
         * @ORM\Column(type="boolean")
         */
        private int $is_active = 0;




        /**
         * User constructor.
         */
        public function __construct()
        {
            $this->roles = self::DEFAULT_ROLES;


        }


        public function getId(): ?int
        {
            return $this->id;
        }

        public function getEmail(): ?string
        {
            return $this->email;
        }

        public function setEmail(string $email): self
        {
            $this->email = $email;

            return $this;
        }

        /**
         * A visual identifier that represents this user.
         *
         * @see UserInterface
         */
        public function getUsername(): string
        {
            return (string) $this->email;
        }

        /**
         * @see UserInterface
         */
        public function getRoles(): array
        {
            $roles = $this->roles;
            // guarantee every user at least has ROLE_USER
           // $roles[] = 'ROLE_USER';
           // return  array_unique(array_merge(['ROLE_USER'],$this->roles));
            return  array_unique($this->roles);
           // return array_unique($roles);
        }

        public function setRoles(array $roles): self
        {
            $this->roles = $roles;
            return  $this;

        }

        /**
         * @see UserInterface
         */
        public function getPassword(): string
        {
            return (string) $this->password;
        }

        public function setPassword(string $password): self
        {
            $this->password = $password;

            return $this;
        }

        /**
         * @see UserInterface
         */
        public function getSalt()
        {
            // not needed when using the "bcrypt" algorithm in security.yaml
        }

        /**
         * @see UserInterface
         */
        public function eraseCredentials()
        {
            // If you store any temporary, sensitive data on the user, clear it here
            // $this->plainPassword = null;
        }

        public function getName(): ?string
        {
            return $this->name;
        }

        public function setName(string $name): self
        {
            $this->name = $name;

            return $this;
        }

        public function getAvatar(): ?string
        {
            return $this->avatar;
        }

        public function setAvatar(?string $avatar): self
        {
            $this->avatar = $avatar;

            return $this;
        }

        public function getIsActive(): ?bool
        {
            return $this->is_active;
        }

        public function setIsActive(bool $is_active): self
        {
            $this->is_active = $is_active;

            return $this;
        }



    }
