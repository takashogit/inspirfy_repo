<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 *
 * @ApiResource(
 *
 *
 * )
 * @ORM\Table(name="architect")
 * @ORM\Entity(repositoryClass="App\Repository\ArchitectRepository" )
 * @ApiFilter(NumericFilter::class,properties={
 *      "user_id":"extract"})
 */
class Architect
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer",name="id_architect")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="licence",length=15, nullable=true)
     */
    private $licence;


    /**
     * @ORM\Column(type="string",name="website_url",length=100, nullable=true)
     */
    private $websiteUrl;
    /**
     * @ORM\Column(type="string",name="description", length=500,nullable=false)
     * @Assert\Length(
     *     min="1",
     *     max="500",
     *     minMessage = "Your URL must be at least {{ limit }} characters long",
     *     maxMessage = "Your URL name cannot be longer than {{ limit }} characters"
     * )
     * @Assert\NotNull();
     */
    private $description;

    /**
     * @ORM\Column(type ="string",name ="certiciction_and_award",length=150, nullable=true )
     */
    private $certicictionAndAward;


    /**
     * @ORM\Column(type="string", name="phone", length=10, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="integer", name="cost_from", length=6, nullable=true)
     */
    private $costFrom;

    /**
     * @ORM\Column(type="integer",name="cost_to",length=6,nullable=true)
     */
    private $costTo;

    /**
     * @ORM\Column(type="integer", name="currency",length=6, nullable=true )
     */
    private $currency;

    /**
     * @ORM\Column(type="string", name="cost_details", length=200, nullable=true)
     */
    private $costDetails;

    /**
     * @ORM\Column(type="string",name="personal_info",length= 100, nullable=true)
     */
    private $personalInfo;

    /**
     * @ORM\Column(type="string",name="link_video", length=50, nullable=true)
     */
    private $linkVideo;

    /**
     *  check if nullable is needed
     * @ORM\Column(type="integer",name="archive_id_archive",length=11, nullable=true)
     */
    private $archiveIdArchive;


    /**
     *
     * @ORM\Column(type="integer", name="user_id",length=11)
     */
    private $user_id;

    /**
     * @ORM\OneToMany(targetEntity=ProjectsArchitects::class, mappedBy="architect_id_architect", orphanRemoval=true)
     *
     */
    private $projectsArchitects;

    public function __construct()
    {
        $this->projectsArchitects = new ArrayCollection();
    }





    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id_architect
     */
    public function setIdArchitect($id_architect): void
    {
        $this->id_architect = $id_architect;
    }


    /**
     * @return mixed
     */
    public function getLicence()
    {
        return $this->licence;
    }

    /**
     * @param mixed $licence
     */
    public function setLicence($licence): void
    {
        $this->licence = $licence;
    }

    /**
     * @return mixed
     */
    public function getWebsiteUrl()
    {
        return $this->websiteUrl;
    }

    /**
     * @param mixed $websiteUrl
     */
    public function setWebsiteUrl($websiteUrl): void
    {
        $this->websiteUrl = $websiteUrl;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCerticictionAndAward()
    {
        return $this->certicictionAndAward;
    }

    /**
     * @param mixed $certicictionAndAward
     */
    public function setCerticictionAndAward($certicictionAndAward): void
    {
        $this->certicictionAndAward = $certicictionAndAward;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getCostFrom()
    {
        return $this->costFrom;
    }

    /**
     * @param mixed $costFrom
     */
    public function setCostFrom($costFrom): void
    {
        $this->costFrom = $costFrom;
    }

    /**
     * @return mixed
     */
    public function getCostTo()
    {
        return $this->costTo;
    }

    /**
     * @param mixed $costTo
     */
    public function setCostTo($costTo): void
    {
        $this->costTo = $costTo;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getCostDetails()
    {
        return $this->costDetails;
    }

    /**
     * @param mixed $costDetails
     */
    public function setCostDetails($costDetails): void
    {
        $this->costDetails = $costDetails;
    }

    /**
     * @return mixed
     */
    public function getPersonalInfo()
    {
        return $this->personalInfo;
    }

    /**
     * @param mixed $personalInfo
     */
    public function setPersonalInfo($personalInfo): void
    {
        $this->personalInfo = $personalInfo;
    }

    /**
     * @return mixed
     */
    public function getLinkVideo()
    {
        return $this->linkVideo;
    }

    /**
     * @param mixed $linkVideo
     */
    public function setLinkVideo($linkVideo): void
    {
        $this->linkVideo = $linkVideo;
    }

    /**
     * @return mixed
     */
    public function getArchiveIdArchive()
    {
        return $this->archiveIdArchive;
    }

    /**
     * @param mixed $archiveIdArchive
     */
    public function setArchiveIdArchive($archiveIdArchive): void
    {
        $this->archiveIdArchive = $archiveIdArchive;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id): void
    {
        $this->user_id = $user_id;

    }
//
//    /**
//     * @return Collection|ProjectsArchitects[]
//     */
//    public function getProjectsArchitects(): Collection
//    {
//        return $this->projectsArchitects;
//    }
//
//    public function addProjectsArchitect(ProjectsArchitects $projectsArchitect): self
//    {
//        if (!$this->projectsArchitects->contains($projectsArchitect)) {
//            $this->projectsArchitects[] = $projectsArchitect;
//            $projectsArchitect->setArchitectIdArchitect($this);
//        }
//
//        return $this;
//    }
//
//    public function removeProjectsArchitect(ProjectsArchitects $projectsArchitect): self
//    {
//        if ($this->projectsArchitects->contains($projectsArchitect)) {
//            $this->projectsArchitects->removeElement($projectsArchitect);
//            // set the owning side to null (unless already changed)
//            if ($projectsArchitect->getArchitectIdArchitect() === $this) {
//                $projectsArchitect->setArchitectIdArchitect(null);
//            }
//        }
//
//        return $this;
//    }






}
