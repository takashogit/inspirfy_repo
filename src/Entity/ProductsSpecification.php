<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * ProductsSpecification
 *
 * @ORM\Table(name="products_specification", uniqueConstraints={@ORM\UniqueConstraint(name="products_specification_hash_product_IDX", columns={"hash_product"})})
 * @ORM\Entity
 * @ApiResource
 */
class ProductsSpecification
{

    /**
     * @var string
     *
     * @ORM\Column(name="hash_product", type="string", length=191, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $hashProduct;

    /**
     * @var string|null
     *
     * @ORM\Column(name="model", type="string", length=255, nullable=true)
     */
    private $model;

    /**
     * @var string|null
     *
     * @ORM\Column(name="product_id", type="string", length=100, nullable=true)
     */
    private $productId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="manufactured", type="string", length=255, nullable=true)
     */
    private $manufactured;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sold", type="string", length=255, nullable=true)
     */
    private $sold;

    /**
     * @var string|null
     *
     * @ORM\Column(name="size_weight", type="string", length=255, nullable=true)
     */
    private $sizeWeight;

    /**
     * @var string|null
     *
     * @ORM\Column(name="assembly_required", type="string", length=255, nullable=true)
     */
    private $assembly_required;

    /**
     * @var string|null
     *
     * @ORM\Column(name="category", type="string", length=255, nullable=true)
     */
    private $category;

    /**
     * @var string|null
     *
     * @ORM\Column(name="style", type="string", length=255, nullable=true)
     */
    private $style;

    /**
     * @var string|null
     *
     * @ORM\Column(name="materials", type="string", length=255, nullable=true)
     */
    private $materials;

    /**
     * @var string|null
     *
     * @ORM\Column(name="collection", type="string", length=255, nullable=true)
     */
    private $collection;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @var string|null
     *
     * @ORM\Column(name="commercial_grade", type="string", length=255, nullable=true)
     */
    private $commercial_grade;

    /**
     * @var Products
     *
     * @ORM\OneToOne(targetEntity="Products", mappedBy="specification")
     * @ORM\JoinColumn(name="hash_product", referencedColumnName="hash_product")
     */

    private $product;

    public function getHashProduct(): ?string
    {
        return $this->hashProduct;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(?string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getProductId(): ?string
    {
        return $this->productId;
    }

    public function setProductId(?string $productId): self
    {
        $this->productId = $productId;

        return $this;
    }

    public function getManufactured(): ?string
    {
        return $this->manufactured;
    }

    public function setManufactured(?string $manufactured): self
    {
        $this->manufactured = $manufactured;

        return $this;
    }

    public function getSold(): ?string
    {
        return $this->sold;
    }

    public function setSold(?string $sold): self
    {
        $this->sold = $sold;

        return $this;
    }

    public function getSizeWeight(): ?string
    {
        return $this->sizeWeight;
    }

    public function setSizeWeight(?string $sizeWeight): self
    {
        $this->sizeWeight = $sizeWeight;

        return $this;
    }

    public function getAssemblyRequired(): ?string
    {
        return $this->assembly_required;
    }

    public function setAssemblyRequired(?string $assembly_required): self
    {
        $this->assembly_required = $assembly_required;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getStyle(): ?string
    {
        return $this->style;
    }

    public function setStyle(?string $style): self
    {
        $this->style = $style;

        return $this;
    }

    public function getMaterials(): ?string
    {
        return $this->materials;
    }

    public function setMaterials(?string $materials): self
    {
        $this->materials = $materials;

        return $this;
    }

    public function getCollection(): ?string
    {
        return $this->collection;
    }

    public function setCollection(?string $collection): self
    {
        $this->collection = $collection;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getCommercialGrade(): ?string
    {
        return $this->commercial_grade;
    }

    public function setCommercialGrade(?string $commercial_grade): self
    {
        $this->commercial_grade = $commercial_grade;

        return $this;
    }

    public function getProduct(): ?Products
    {
        return $this->product;
    }

    public function setProduct(?Products $product): self
    {
        $this->product = $product;

        return $this;
    }


}
