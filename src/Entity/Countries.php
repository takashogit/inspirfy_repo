<?php
    declare(strict_types=1);
    namespace App\Entity;

    use ApiPlatform\Core\Annotation\ApiFilter;
    use ApiPlatform\Core\Annotation\ApiProperty;
    use Doctrine\Common\Collections\ArrayCollection;
    use Doctrine\ORM\Mapping as ORM;
    use ApiPlatform\Core\Annotation\ApiResource;
    use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
    use Symfony\Component\Serializer\Annotation\Groups;
    use ApiPlatform\Core\Annotation\ApiSubresource;
    use Doctrine\Common\Collections\Collection;

    /**
     *
     * @ApiResource(
     *           normalizationContext={"groups"={"read"}},
     *             denormalizationContext={"groups"={"write"}},
     *      itemOperations={"get"={"access_control"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')"}},
     *       collectionOperations={
     *          "get"={
     *              "method"="GET",
     *              "swagger_context" = {
     *                  "parameters" = {
     *                      {
     *                          "name" = "name",
     *                          "in" = "query",
     *                          "description" = "DSAS",
     *                          "required" = "true",
     *                          "type" : "string",
     *
     *                      }
     *                  }
     *               }
     *          }
     *     }
     * )
     *
     * Class Countries
     * @package App\Entity
     * @ORM\Entity()
     * @ORM\Table(name="adr_countries")
     * @ApiFilter(SearchFilter::class, properties={"name":"start",
     *    "regions.name":"start" })
     *
     */
    class Countries
    {
        /**
         * @var int
         * @ORM\Id()
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         * @ApiProperty(identifier=true)
         * @Groups({"read"})
         *
         */
        private int $id;

        /**
         * @var string
         * @ORM\Column(name="name", type="string", length=255)
         * @ORM\ManyToOne(targetEntity="Project", inversedBy="adr_countries_id")
         * @Groups({"read"})
         *
         */
        private string $name;


        /**
         * @var string
         * @ORM\Column(name="code", type="string", length=10)
         * @Groups({"read"})
         */
        private $code;

        /**
         * @var Regions
         * @ORM\OneToMany(targetEntity="Regions", mappedBy="countries")
         * @ApiSubresource()
         * @Groups({"read"})
         */
        private $regions;


        /**
         * Countries constructor.
         */
        public function __construct()
        {
            $this->regions = new ArrayCollection();
        }


        public function getId()
        {
            return $this->id;
        }


        public function getName(): ?string
        {
            return $this->name;
        }

        public function setName($name): void
        {
            $this->name = $name;
        }

        public function getCode()
        {
            return $this->code;
        }


        public function setCode($code): void
        {
            $this->code = $code;
        }

        /**
         * @return Collection | Regions[]
         */
        public function getRegions(): ?Collection
        {
            return $this->regions;
        }


        public function setRegions(?Regions $regions): self
        {
            $this->regions = $regions->getName();

            return $this;

        }


    }