<?php

    namespace App\Entity;

    use ApiPlatform\Core\Annotation\ApiFilter;
    use ApiPlatform\Core\Annotation\ApiResource;
    use Doctrine\ORM\Mapping as ORM;
    use App\Controller\CardController;
    use Symfony\Component\Serializer\Annotation\Groups;
    use ApiPlatform\Core\Annotation\ApiProperty;
    use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;


    /**
     *
     * @ApiResource(
     *     normalizationContext={"groups"={"read"}},
     *   collectionOperations={
     *   "post"={
     *      "controller"=CardController::class,
     *      "deserialize"=false,
     *     }
     *   }
     *
     * )
     *
     * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
     * @ORM\Table(name="`order`")
     * @ApiFilter(SearchFilter::class, properties={"user_id":"exact"})
     *
     */
    class Order
    {
        /**
         * @var int
         * @ORM\Id()
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         * @Groups("read")
         *
         */
        private $id;

        /**
         * @var string
         * @Groups({"read"})
         * @ORM\Column(name="hash_product", type="string", length=32)
         */
        private $hash_product;

        /**
         * @var int
         *
         * @ORM\Column(type="integer")
         *
         */
        private $price;

        /**
         * @var int
         *
         * @ORM\Column(type="integer")
         * @Groups({"read"})
         *
         */
        private $quantity;

        /**
         * @var int
         *
         * @ORM\Column(type="integer")
         * @ApiProperty(identifier=true)
         * @Groups({"read"})
         *
         */
        private $user_id;

        /**
         * @var \DateTime
         *
         * @ORM\Column(type="datetime")
         *
         */
        private $create_time;

        /**
         * @var \DateTime
         *
         * @ORM\Column(type="datetime")
         */
        private $update_time;

        /**
         *
         * @var  \DateTime
         *
         * @ORM\Column(type="integer")
         */
        private $order_status;

        /**
         * @var string
         * @ORM\Column(type="string")
         * @Groups({"read"})
         */
        private $order_num;


        /**
         * Order constructor.
         */
        public function __construct()
        {
            $this->create_time = new \DateTime('now', new \DateTimeZone('Europe/Kiev'));
            $this->update_time = new \DateTime('NOW');
        }


        public function getId(): ?int
        {
            return $this->id;
        }

        public function getHashProduct(): ?string
        {
            return $this->hash_product;
        }

        public function setHashProduct(string $hash_product): self
        {
            $this->hash_product = $hash_product;

            return $this;
        }

        public function getPrice(): ?int
        {
            return $this->price;
        }

        public function setPrice(int $price): self
        {
            $this->price = $price;

            return $this;
        }

        public function getQuantity(): ?int
        {
            return $this->quantity;
        }

        public function setQuantity(int $quantity): self
        {
            $this->quantity = $quantity;

            return $this;
        }

        public function getUser(): ?int
        {
            return $this->user_id;
        }

        public function setUser($user_id): self
        {
            $this->user_id = $user_id;

            return $this;
        }

        public function getCreateTime(): ?\DateTimeInterface
        {
            return $this->create_time;
        }

        public function setCreateTime(\DateTimeInterface $create_time): self
        {
            $this->create_time = $create_time;

            return $this;
        }

        public function getUpdateTime(): ?\DateTimeInterface
        {
            return $this->update_time;
        }


        public function getOrderStatus(): ?int
        {
            return $this->order_status;
        }

        public function setOrderStatus(int $order_status): self
        {
            $this->order_status = $order_status;

            return $this;
        }




        public function getUserId(): ?int
        {
            return $this->user_id;
        }

        public function setUserId(int $user_id): self
        {
            $this->user_id = $user_id;

            return $this;
        }

        public function setUpdateTime(\DateTimeInterface $update_time): self
        {
            $this->update_time = $update_time;

            return $this;
        }

        /**
         * @return string
         */
        public function getOrderNum(): string
        {
            return $this->order_num;
        }

        /**
         * @param string $order_num
         */
        public function setOrderNum(string $order_num): void
        {
            $this->order_num = $order_num;
        }


    }
