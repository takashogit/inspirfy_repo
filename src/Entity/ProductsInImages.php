<?php


namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class ProductsInImages
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="product_in_picture")
 * @ApiResource(
 *
 * )
 * @ApiFilter(NumericFilter::class, properties={
 *  "projectImagesId":"extract"
 *     })
 *
 */
class ProductsInImages
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * Groups({"prodImage"})
     */
    private $id;
    /**
     * @ORM\Column(type="integer",name="project_images_id")
     *  Groups({"prodImage"})
     *
     */
    private $projectImagesId;
    /**
     * @ORM\Column(type="integer",name="cube_number", nullable=false)
     * Groups({"prodImage"})
     */
    private $cubeNumber;
    /**
     * @ORM\Column(type="string",name="products_hash_product", nullable=false)
     * Groups({"prodImage"})
     */
    private $productsHashProduct;



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProjectImagesId()
    {
        return $this->projectImagesId;
    }

    /**
     * @param mixed $projectImagesId
     */
    public function setProjectImagesId($projectImagesId): void
    {
        $this->projectImagesId = $projectImagesId;
    }

    /**
     * @return mixed
     */
    public function getCubeNumber()
    {
        return $this->cubeNumber;
    }

    /**
     * @param mixed $cubeNumber
     */
    public function setCubeNumber($cubeNumber): void
    {
        $this->cubeNumber = $cubeNumber;
    }

    /**
     * @return mixed
     */
    public function getProductsHashProduct()
    {
        return $this->productsHashProduct;
    }

    /**
     *
     * @param $productsHashProduct
     */
    public function setProductsHashProduct($productsHashProduct): void
    {
        $this->productsHashProduct = $productsHashProduct;
    }
}