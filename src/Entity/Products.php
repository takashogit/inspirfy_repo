<?php
declare(strict_types=1);
namespace App\Entity;

use Doctrine\{Common\Collections\ArrayCollection, Common\Collections\Collection, ORM\Mapping as ORM};
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\{ApiFilter, ApiResource, ApiProperty};
use ApiPlatform\Core\Bridge\Doctrine\Orm\{Filter\SearchFilter, Filter\OrderFilter};

/**
 * Products
 *
 * @ORM\Table(name="products", uniqueConstraints={@ORM\UniqueConstraint(name="product_specification_hash_product_IDX", columns={"hash_product"})})
 * @ORM\Entity(repositoryClass="App\Repository\ProductsRepository")
 * @ApiResource(
 *     iri="https://schema.org/Product",
 *     normalizationContext={"groups"={"read","description","category","images"}},
 *     denormalizationContext={"groups"={"write"}},
 *     attributes={
 *          "pagination_items_per_page": 30
 *     },
 *     collectionOperations={
 *          "get",
 *          "post"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"security"="is_granted('ROLE_ADMIN') or object.owner == user"},
 *         "delete"={"security"="is_granted('ROLE_ADMIN') or object.owner == user"},
 *         "patch"={"security"="is_granted('ROLE_ADMIN') or object.owner == user"},
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={
 *     "category.hashParent": "exact",
 *     "category.hash": "exact",
 *     "specification.materials": "exact",
 *     "specification.manufactured": "exact",
 *     "specification.sold": "exact",
 *     "specification.assembly_required": "exact",
 *     "specification.style": "exact",
 *     "specification.collection": "exact",
 *     "specification.color": "exact",
 *     "specification.commercial_grade": "exact",
 *     "title":"start"
 * })
 * @ApiFilter(OrderFilter::class, properties={
 *     "title",
 *     "price"
 * }, arguments={"orderParameterName"="order"})
 */
class Products
{
    /**
     * @var string
     *
     * @ORM\Column(name="hash_product", type="string", length=191, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"read"})
     *
     */
    private  $hashProduct;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     * @Groups({"read"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="string", length=255, nullable=false)
     * @Groups({"read"})
     */
    private  $price;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=false)
     * @Groups({"read"})
     */
    private  $link;

    /**
     * @var ProductsDescription
     *
     * @ORM\OneToOne(targetEntity="ProductsDescription", mappedBy="product")
     * @Groups({"description"})
     */
    private $description;

    /**
     * @var ProductImg
     * @ORM\OneToMany(targetEntity="ProductImg", mappedBy="products")
     * @Groups({"read", "images"})
     */
    private $images;

    /**
     * @var Category*
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="products")
     * @ORM\JoinTable(
     *     name="products_in_category",
     *     joinColumns={
     *         @ORM\JoinColumn(name="hash_product", referencedColumnName="hash_product")
     *     },
     *     inverseJoinColumns={
     *         @ORM\JoinColumn(name="hash_category", referencedColumnName="hash")
     *     }
     * )
     * @Groups({"read", "category"})
     *
     */
    private $category;

    /**
     * @var ProductsSpecification
     * @ORM\OneToOne(targetEntity="ProductsSpecification", mappedBy="product")
     * @Groups({"read","specification"})
     */
    private  $specification;

    /**
     * Products constructor.
     */

    public function __construct()
    {
        $this->category = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public function getHashProduct(): ?string
    {
        return $this->hashProduct;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getDescription(): ?ProductsDescription
    {
        return $this->description;
    }

    public function setDescription(?ProductsDescription $description): self
    {
        $this->description = $description;

        // set (or unset) the owning side of the relation if necessary
        $newProduct = null === $description ? null : $this;
        if ($description->getProduct() !== $newProduct) {
            $description->setProduct($newProduct);
        }

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
        }

        return $this;
    }

    /**
     *
     * @return Collection|ProductImg[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(ProductImg $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setProducts($this);
        }

        return $this;
    }

    public function removeImage(ProductImg $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getProducts() === $this) {
                $image->setProducts(null);
            }
        }

        return $this;
    }

    public function getSpecification(): ?ProductsSpecification
    {
        return $this->specification;
    }

    public function setSpecification(?ProductsSpecification $specification): self
    {
        $this->specification = $specification;

        // set (or unset) the owning side of the relation if necessary
        $newProduct = null === $specification ? null : $this;
        if ($specification->getProduct() !== $newProduct) {
            $specification->setProduct($newProduct);
        }

        return $this;
    }


}
