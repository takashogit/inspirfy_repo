<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\ProductsFilterGetIdxController;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Category;

/**
 * Class ProductsFilters
 * @package App\Entity
 * @ApiResource(
 *     collectionOperations={
 *     },
 *     attributes={"pagination_enabled"=false},
 *     itemOperations={
 *          "get"={
 *               "controller"=ProductsFilterGetIdxController::class,
 *           }
 *     }
 * )
 *
 */
class ProductsFilters
{

    /**
     * @var Category
     * @ApiProperty(identifier=true)
     */
    private $hash_category;

    /**
     * @var array
     */
    public $materials;
    /**
     * @var array
     */
    public $manufactured;
    /**
     * @var array
     */
    public $sold;
    /**
     * @var array
     */
    public $assembly_required;
    /**
     * @var array
     */
    public $style;
    /**
     * @var array
     */
    public array $collection;
    /**
     * @var array
     */
    public $color;
    /**
     * @var array
     */
    public $commercial_grade;

    /**
     * @param string $hash_category
     * @return $this
     */
    public function setHashCatecory(string $hash_category): self
    {
        $this->hash_category = $hash_category;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHashCategory(): ?string
    {
        return $this->hash_category;
    }
}