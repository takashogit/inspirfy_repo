<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * ProductsInCategory
 *
 * @ORM\Table(name="products_in_category", uniqueConstraints={@ORM\UniqueConstraint(name="products_in_category_hash_string_IDX", columns={"hash_string"})})
 * @ORM\Entity
 * @ApiResource
 */
class ProductsInCategory
{
    /**
     * @var string
     *
     * @ORM\Column(name="hash_string", type="string", length=191, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $hashString;

    /**
     * @var string
     *
     * @ORM\Column(name="hash_category", type="string", length=191, nullable=false)
     */
    private $hashCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="hash_product", type="string", length=191, nullable=false)
     */
    private $hashProduct;

    /**
     * @return string|null
     */
    public function getHashString(): ?string
    {
        return $this->hashString;
    }

    public function getHashCategory(): ?string
    {
        return $this->hashCategory;
    }

    public function setHashCategory(string $hashCategory): self
    {
        $this->hashCategory = $hashCategory;

        return $this;
    }

    public function getHashProduct(): ?string
    {
        return $this->hashProduct;
    }

    public function setHashProduct(string $hashProduct): self
    {
        $this->hashProduct = $hashProduct;

        return $this;
    }


}
