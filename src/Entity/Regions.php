<?php

    namespace App\Entity;

    use ApiPlatform\{Core\Annotation\ApiResource, Core\Annotation\ApiSubresource, Core\Annotation\ApiProperty};
    use Doctrine\Common\Collections\ArrayCollection;
    use Doctrine\Common\Collections\Collection;
    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Serializer\Annotation\Groups;


    /**
     * @ApiResource(
     *    normalizationContext={"groups"={"read"}},
     *     denormalizationContext={"groups"={"write"}},
     *      itemOperations={"get"={"access_control"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')"}},
     *     collectionOperations={"get"},
     *
     *
     * )
     * Class Regions
     * @package App\Entity
     * @ORM\Entity()
     * @ORM\Table(name="adr_regions")
     *
     *
     */
    class Regions
    {
        /**
         * @var int
         * @ORM\Id()
         * @ORM\Column(type="integer")
         * @ApiProperty(identifier=true)
         * @ORM\GeneratedValue(strategy="AUTO")
         * @Groups("read")
         */
        private $id;

        /**
         * @var string
         * @ORM\Column( name="name", type="string",length=255)
         * @Groups("read")
         */
        private $name;

        /**
         * @var string
         * @ORM\Column(name="code",type="string",length=10)
         * @Groups("get")
         */
        private $code;

        /**
         * @var int
         * @ORM\Column(name="country_id",type="integer")
         * @Groups("get")
         */

        private $country_id;

        /**
         * @var Countries
         * @ORM\ManyToOne(targetEntity="Countries", inversedBy="regions",cascade={"persist"})
         * @ORM\JoinColumn(name="country_id")
         *
         */

        private $countries;

        /**
         * @var Cities
         * @ORM\OneToMany(targetEntity="Cities", mappedBy="city")
         * @ApiSubresource()
         * @Groups("write")
         */
        private $cities;

        /**
         * Regions constructor.
         */
        public function __construct()
        {
            $this->cities = new ArrayCollection();
        }


        public function getId()
        {
            return $this->id;
        }


        public function getName()
        {
            return $this->name;
        }


        public function setName($name): void
        {
            $this->name = $name;
        }


        public function getCode()
        {
            return $this->code;
        }


        public function setCode($code): void
        {
            $this->code = $code;
        }


        public function getCountryId()
        {
            return $this->country_id;
        }


        public function setCountryId($country_id): void
        {
            $this->country_id = $country_id;
        }

        /**
         * @return Collection | Cities[]
         */
        public function getCities(): ?Collection
        {
            return $this->cities;
        }


        public function setCities(?Cities $cities): self
        {
            $this->cities = $cities;

            return $this;

        }


    }