<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CategoryArchRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      collectionOperations={
 *          "get",
 *          "post"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"security"="is_granted('ROLE_ADMIN') or object.owner == user"},
 *         "delete"={"security"="is_granted('ROLE_ADMIN') or object.owner == user"},
 *         "patch"={"security"="is_granted('ROLE_ADMIN') or object.owner == user"},
 *     }
 * )
 * @ORM\Entity(repositoryClass=CategoryArchRepository::class)
 * @ORM\Table(name="category_arch")
 */
class CategoryArch
{


    /**
     * @ORM\Id()
     * @ORM\Column(name="style_id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private ?int $id;

    /**
     *
     * @ORM\Column(type="string", name="style_name",  length=45)
     */
    private ?string $style_name;


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return string|null
     */
    public function getStyleName(): ?string
    {
        return $this->style_name;
    }

    /**
     * @param string $style_name
     * @return $this
     */
    public function setStyleName(string $style_name): self
    {
        $this->style_name = $style_name;

        return $this;
    }
}
