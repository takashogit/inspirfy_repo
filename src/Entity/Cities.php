<?php


    namespace App\Entity;


    use ApiPlatform\Core\Annotation\ApiResource;
    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Serializer\Annotation\Groups;

    /**
     * @ApiResource(
     *      attributes={"pagination_enabled"=false},
     *     normalizationContext={"groups"={"get"}},
     *      itemOperations={"get"},
     *     collectionOperations={"get"}
     * )
     * Class Cities
     * @package App\Entity
     * @ORM\Entity()
     * @ORM\Table(name="adr_cities")
     */
    class Cities
    {
        /**
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         * @ORM\Column(type="integer")
         * @Groups({"read","get"})
         */

        private $id;
        /**
         * @ORM\Column(type="integer")
         */
        private $region_id;

        /**
         * @ORM\Column(type="integer")
         */
        private $country_id;

        /**
         * @ORM\Column(type="decimal")
         */
        private $latitude;

        /**
         * @ORM\Column(type="decimal")
         */
        private $longitude;

        /**
         * @ORM\Column(type="string",length=255)
         * @Groups({"get"})
         */
        private $name;

        /**
         * @var Regions
         * @ORM\ManyToOne(targetEntity="Regions",inversedBy="cities")
         * @ORM\JoinColumn(name="region_id")
         *
         */
        private $city;


        public function getId()
        {
            return $this->id;
        }


        public function getRegionId()
        {
            return $this->region_id;
        }


        public function setRegionId($region_id): void
        {
            $this->region_id = $region_id;
        }


        public function getCountryId()
        {
            return $this->country_id;
        }


        public function setCountryId($country_id): void
        {
            $this->country_id = $country_id;
        }


        public function getLatitude()
        {
            return $this->latitude;
        }


        public function setLatitude($latitude): void
        {
            $this->latitude = $latitude;
        }


        public function getLongitude()
        {
            return $this->longitude;
        }


        public function setLongitude($longitude): void
        {
            $this->longitude = $longitude;
        }


        public function getName()
        {
            return $this->name;
        }


        public function setName($name): void
        {
            $this->name = $name;
        }


    }