<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProjectsArchitectsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\Architectsprojects;

/**
 * @package  App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="projects")
 * @ApiResource(
 *           collectionOperations={
 *               "get"={
 *                   "controller"=Architectsprojects::class ,
 *                    "deserialize"=false,
 *                      }
 *              }
 * )
 *
 */
class ProjectsArchitects
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer",name="idprojects")
     */
    private $id;

    /**
     *
     *
     * @ORM\Column(type="integer", name="architect_id_architect")
     * @Groups("project")
     *
     */
    private $architectIdArchitect;

    /**
     * @ORM\Column(type="integer",name="project_id_project" )
     */
    private $projetIdProjet;


    
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get List Architect
     * @return mixed
     */
    public function getArchitectIdArchitect()
    {

        return $this->architectIdArchitect;
    }

    /**
     *  List Architect Project
     * @param mixed $architectIdArchitect
     */
    public function setArchitectIdArchitect( $architectIdArchitect): void
    {
        $this->architectIdArchitect = $architectIdArchitect;
    }


    /**
     * Get List Architect Project
     * @return mixed
     */
    public function getProjetIdProjet()
    {
        return $this->projetIdProjet;
    }

    /**
     * Set List Architect Project
     * @param mixed $projetIdProjet
     */
    public function setProjetIdProjet($projetIdProjet): void
    {
        $this->projetIdProjet = $projetIdProjet;
    }
}
