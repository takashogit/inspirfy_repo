<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CostRangeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CostRangeRepository::class)
 * @ORM\Table(name="cost_range")
 * @ApiResource(normalizationContext={"groups"={"range"}}
 *     )
 *
 */
class CostRange
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @Groups({"range"});
     */
    private $id;

    /**
     * @ORM\Column(type="integer",name="min")
     */
    private $min;

    /**
     * @ORM\Column(type="integer",name="max")
     */
    private $max;


    /**
     *
     * @Groups({"range"});
     */
    private  $res;

    /**
     * @return mixed
     */
    public function getRes()
    {
        return $this->min."-".$this->max;
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMin(): ?int
    {
        return $this->min;
    }

    public function setMin(int $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getMax(): ?int
    {
        return $this->max;
    }

    public function setMax(int $max): self
    {
        $this->max = $max;

        return $this;
    }
}
