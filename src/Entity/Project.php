<?php
declare(strict_types=1);
namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use App\Controller\ProjectImageAction;


/**
 * Project
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository", repositoryClass=ProjectRepository::class)
 * @ORM\Table(name="project")
 * @ApiResource(
 *
 *  normalizationContext={"groups"={"project"}},
 *  denormalizationContext={"groups"={"project"}},
 *     itemOperations={
 *           "get",
 *         "put"={"security"="is_granted('ROLE_ARCHITECT') or object.owner == user"},
 *         "delete"={"security"="is_granted('ROLE_ARCHITECT) or object.owner == user"},
 *         "patch"={"security"="is_granted('ROLE_ARCHITECT') or object.owner == user"},
 *     },
 *     collectionOperations={
 *           "get"={},
 *          "post"={
 *              "security"="is_granted('ROLE_ARCHITECT')",
 *              "method"="POST",
 *              "path"="/project_create",
 *              "controller"=ProjectImageAction::class,
 *              "deserialize"=false,
 *               "validation_groups"={"project_create"},
 *              "default"={"_api_recive"=false},
 *              "openapi_context"={
 *                   "requestBody" ={"content"={
 *                      "multipart/form-data"={
 *                          "schema"={
 *                              "type"="object","properties"={
 *                                      "file"={"type"="string","format"="binary" }
 *                                                                  }
 *                                                             }
 *                                                       }
 *                                              }
 *                                       }
 *                                   }
 *                            }
 *                       }
 * )
 * @Vich\Uploadable()
 */

class Project
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer",name="id_project")
     * @Groups("project")
     */
    private $id;


    /**
     * @var string|null Name project
     * @param string $name - name of  project
     * @ORM\Column(type="string", name="name", length=50, nullable=false)
     * @Assert\NotNull()
     * @Groups("project")
     *
     *
     */
    private ?string $name;

    /**
     * @var  string|null architect zipcode
     * @ORM\Column(type="string",name="zipcode", length=50)
     * @Assert\NotBlank()
     * @Groups("project")
     *
     */
    private ?string $zipcode;

    /**
     * @var  string   return id style
     * @ORM\Column(type="string",name="style_id")
     *@Groups("project")
     */
    private ?string $styleId;

    /**
     * @var  integer
     * @ORM\Column(type="integer",name="cost")
     * @Groups("project")
     */
    private ?int $cost;

    /**
     * @ORM\Column(type="string",name="project_style", length=50)
     * @Groups("project")
     */
    private ?string $projectStyle;

    /**
     * @ORM\Column(type="json",name="tags")
     * @Groups("project")
     */
    private array $tags = [];

    /**
     * @ApiProperty(iri ="https://schema.org/description")
     * @ORM\Column(type="text",name="project_description", length=590)
     * @Groups("project")
     */
    private  $projectDescription;

    /**
     * @ORM\Column(type="integer", name="adr_countries_id")
     * @ORM\OneToMany(targetEntity="Countries", mappedBy="name")
     * @ORM\JoinColumn(name="adr_countries_id", referencedColumnName="id")
     * @Groups({"read","project"})
     */
    private $adr_countries_id;

    /**
     * @ORM\Column(type="integer",name="adr_cities_id")
     * @Groups("project")
     */
    private $adr_cities_id;


    /**
     * @ORM\Column(type="string", name="poster" )
     * @Groups("project")
     */
    private ?string $poster;

    /**
     * @Vich\UploadableField(mapping="design_projects", fileNameProperty="poster")
     * Assert\NotNull(groups={"project_img_upload"})
     *
     */
    private $file;

    /**
     * @Groups("project")
     */
    private $miniPoster;


    private $data;

    /**
     * ORM\OneToMany(targetEntity=ProjectsArchitects::class, mappedBy="project_id_project", orphanRemoval=true)
     * @Groups("project")
     */
    private $projectsArchitects;


    /**
     * Project constructor.
     */
    public function __construct()
    {
        // $this->adr_countries_id = new ArrayCollection();
        $this->projectsArchitects = new ArrayCollection();



    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getStyleId(): ?string
    {
        return  $this->styleId;
    }

    public function setStyleId(string $styleId): self
    {

        $this->styleId = $styleId;

        return $this;
    }

    public function getCost(): ?int
    {
        return $this->cost;
    }

    public function setCost( $cost): self
    {
        if(!is_int($cost))
            $cost=(int)$cost;
        $this->cost = $cost;
        return  $this;
    }

    public function getProjectStyle(): ?string
    {
        return $this->projectStyle;
    }

    public function setProjectStyle($projectStyle): self
    {
        $this->projectStyle = $projectStyle;

        return $this;
    }

    public function getTags(): ?array
    {
        return $this->tags;
    }

    public function setTags( $tags): self
    {
        $this->tags = explode(',',$tags);
        return $this;
    }

    public function getProjectDescription(): ?string
    {
        return $this->projectDescription;
    }

    public function setProjectDescription(string $projectDescription): self
    {
        $this->projectDescription = $projectDescription;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdrCountriesId()
    {
        return $this->adr_countries_id;
    }

    /**
     * @param mixed $adr_countries_id
     * @return
     */
    public function setAdrCountriesId($adr_countries_id): void
    {
        $this->adr_countries_id =(int) $adr_countries_id;
    }

    /**
     * @return mixed
     */
    public function getAdrCitiesId()
    {
        return $this->adr_cities_id;
    }

    /**
     * @param mixed $adr_cities_id
     */
    public function setAdrCitiesId($adr_cities_id): void
    {
        $this->adr_cities_id =(int) $adr_cities_id;
    }



    /**
     * @return mixed
     */
    public function getMiniPoster()
    {
        return '/static/projects/mini/' . $this->poster;
    }


    public function getPoster(): ?string
    {
        return '/static/projects/' .$this->poster;
    }


    public function setPoster($poster): self
    {
        $this->poster =  $poster;

        return $this;
    }


    public function getFile()
    {
        return $this->file;
    }


    public function setFile($file): void
    {
        $this->file = $file;
    }


    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->getName() . $this->data;
    }









}
