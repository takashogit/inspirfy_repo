<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StyleArchRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"security"="is_granted('ROLE_ADMIN') or object.owner == user"},
 *         "delete"={"security"="is_granted('ROLE_ADMIN') or object.owner == user"},
 *         "patch"={"security"="is_granted('ROLE_ADMIN') or object.owner == user"},
 *     }
 * )
 * @ORM\Entity(repositoryClass=StyleArchRepository::class)
 * @ORM\Table(name="style_arch")
 */
class StyleArch
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="arch_style", length=50)
     *
     */
    private $arch_style;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getArchStyle(): ?string
    {
        return $this->arch_style;
    }

    /**
     * @param string $arch_style
     * @return $this
     */
    public function setArchStyle(string $arch_style): self
    {
        $this->arch_style = $arch_style;

        return $this;
    }
}
