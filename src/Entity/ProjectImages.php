<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\{ApiFilter, ApiResource, ApiSubresource};
use App\Repository\ProjectImagesRepository;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use App\Controller\UploadImageAction;

/**
 *  Architect  image upload
 * @ORM\Entity(repositoryClass=ProjectImagesRepository::class)
 * @ApiResource(
 *  attributes={"order"={"id": "DESC"}},
 *  normalizationContext={"groups"={"prodImage"}},
 *     collectionOperations={
 *           "get"={},
 *          "post"={
 *              "method"="POST",
 *              "path"="/project_images_upload",
 *              "controller"=UploadImageAction::class,
 *              "deserialize"=false,
 *               "validation_groups"={"project_img_upload"},
 *              "default"={"_api_recive"=false},
 *              "openapi_context"={
 *                   "requestBody" ={"content"={
 *                      "multipart/form-data"={
 *                          "schema"={
 *                              "type"="object","properties"={
 *                                      "file"={"type"="string","format"="binary" }
 *                                                                  }
 *                                                             }
 *                                                       }
 *                                              }
 *                                       }
 *                                   }
 *                            }
 *                       }
 * )
 * @ApiFilter(NumericFilter::class, properties={
 *      "projectId":"extract"
 *     })
 *
 * @Vich\Uploadable()
 */
class ProjectImages
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *  @Groups({"prodImage"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer",name="project_id")
     * @Groups({"prodImage"})
     */
    private  $projectId;

    /**
     * @Vich\UploadableField(mapping="design_projects", fileNameProperty="link_image")
     * Assert\NotNull(groups={"project_img_upload"})
     * @Assert\Image(
     *      mimeTypes = "image/*",
     *      minWidth = 200,
     *      minHeight = 200,
     * )
     *
     */
    private $file;

    /**
     * @ORM\Column(type="string", name="link_image", length=50)
     * @Groups({"prodImage"})
     */
    private  $linkImage;

    /**
     * @ORM\Column(type="string",name="title", length=45)
     *  @Groups({"prodImage"})
     */
    private ?string $title;

    /**
     * @ORM\Column(type="string", name="category_id")
     *  @Groups({"prodImage"})
     */
    private ?int $categoryId;

    /**
     * @ORM\Column(type="string", name="style_id",length=11)
     * @Groups({"prodImage"})
     */
    private $styleId;

    /**
     * @var $productsInPicture
     * ORM\OneToMany(targetEntity="ProductsInImages", mappedBy="projectimages")
     * Groups({"prodImage","prodImagePost"})
     * ApiSubresource()
     *
     */
    private  $productsInPicture;


    /**
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id_project")
     *
     */
  //  private  $nameProgect;


    /**
     * ProductImg constructor.
     */
    public function __construct()
    {
        $this->productsInPicture = new ArrayCollection();
       // $this->nameProgect = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getProjectId(): ?int
    {
        return  $this->projectId;
    }

    public function setProjectId(int $projectId): self
    {
        $this->projectId = !empty($projectId) ? $projectId : 0;


        return $this;
    }

    public function getLinkImage(): ?string
    {
        return $this->linkImage;
    }

    public function setLinkImage( $linkImage): self
    {
        $this->linkImage = '/static/projects/'.$linkImage;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCategoryId(): ?string
    {
        return $this->categoryId;
    }

    public function setCategoryId( $categoryId): self
    {

        $this->categoryId = !empty((int)$categoryId) ? (int)$categoryId : 0;

        return $this;
    }

    public function getStyleId(): ?int
    {
        return $this->styleId;
    }

    public function setStyleId( $style_id): self
    {

        $this->styleId = !empty($style_id) ? $style_id : 0;

        return $this;
    }


    public function getFile()
    {
        return $this->file;
    }


    public function setFile($file): void
    {
        $this->file = $file;
    }

    /**
     * @return Collection | ProductsInImages[]
     */

    public function getProductsInPicture():?Collection
    {
        return $this->productsInPicture;
    }


    public function setProductsInPicture(? ProductsInImages $productsInImages): void
    {
        $this->productsInPicture = $productsInImages->getProductsHashProduct();
    }

}
