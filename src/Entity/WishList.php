<?php

    namespace App\Entity;

    use ApiPlatform\Core\Annotation\ApiResource;
    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ApiResource(
     *       itemOperations={
     *     "get"={"security"="is_granted('ROLE_ADMIN') or object.owner == user"},
     *     "delete" ={"security"="is_granted('ROLE_ADMIN') or object.owner == user"}
     *     },
     *     collectionOperations={
     *     "post"}
     * )
     * @ORM\Entity(repositoryClass="App\Repository\WishListRepository")
     */
    class WishList
    {
        /**
         * @ORM\Id()
         * @ORM\GeneratedValue(strategy="AUTO")
         * @ORM\Column(type="integer")
         */
        private ?int $id;

        /**
         * @ORM\Column(type="integer",name="user_id")
         */
        private ?int $user_id;

        /**
         * @ORM\Column(type="string", name="hash_product", length=32)
         */
        private ?string $hash_product;

        public function getId(): ?int
        {
            return $this->id;
        }

        /**
         * @return int|null
         */
        public function getUserId(): ?int
        {
            return $this->user_id;
        }

        /**
         * @param int $user_id
         * @return $this
         */
        public function setUserId(int $user_id): self
        {
            $this->user_id = $user_id;

            return $this;
        }

        /**
         * @return string|null
         */
        public function getHashProduct(): ?string
        {
            return $this->hash_product;
        }

        /**
         * @param string $hash_product
         * @return $this
         */
        public function setHashProduct(string $hash_product): self
        {
            $this->hash_product = $hash_product;

            return $this;
        }
    }
