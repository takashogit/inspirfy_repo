<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TagsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=TagsRepository::class)
 * @ORM\Table(name="tags")
 */
class Tags
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO" )
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer",name="id_project")
     */
    private $id_project;

    /**
     * @ORM\Column(type="string",name="title", length=45)
     */
    private $title;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdProject(): ?int
    {
        return $this->id_project;
    }

    public function setIdProject(int $id_project): self
    {
        $this->id_project = $id_project;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
