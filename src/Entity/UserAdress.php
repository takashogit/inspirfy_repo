<?php

    namespace App\Entity;

    use ApiPlatform\{Core\Annotation\ApiFilter,
        Core\Annotation\ApiResource,
        Core\Bridge\Doctrine\Orm\Filter\SearchFilter};
    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ApiResource()
     * @ORM\Entity(repositoryClass="App\Repository\UserAdressRepository")
     * @ApiFilter(SearchFilter::class, properties={ "user_id": "exact",})
     */
    class UserAdress
    {
        /**
         * @ORM\Id()
         * @ORM\GeneratedValue()
         *
         * @ORM\Column(type="integer")
         */
        private $id;

        /**
         *
         * @ORM\Column(type="integer")
         */
        private $user_id;

        /**
         * @ORM\Column(type="integer", length=155)
         */
        private $country;

        /**
         * @ORM\Column(type="integer", length=100)
         */
        private $state;

        /**
         * @ORM\Column(type="integer", length=100)
         *
         */
        private $city;


        /**
         * @ORM\Column(type="string", length=150)
         */

        private string $adress;


        /**
         * @ORM\Column(type="integer", nullable=true)
         */
        private $post_code;

        /**
         * @ORM\Column(type="string", length=100)
         */
        private $phone;

        /**
         * @ORM\Column(type="string", length=255)
         */
        private $street;

        /**
         * @ORM\Column(type="boolean")
         */
        private $defaultShipAddress;


        public function getId(): ?int
        {
            return $this->id;
        }

        public function getUserId(): ?int
        {
            return $this->user_id;
        }

        public function setUserId(int $user_id): self
        {
            $this->user_id = $user_id;

            return $this;
        }

        public function getCountry(): ?string
        {
            return $this->country;
        }

        public function setCountry(string $country): self
        {
            $this->country = $country;

            return $this;
        }

        public function getState(): ?string
        {
            return $this->state;
        }

        public function setState(string $state): self
        {
            $this->state = $state;

            return $this;
        }

        public function getCity(): ?string
        {
            return $this->city;
        }

        public function setCity(string $city): self
        {
            $this->city = $city;

            return $this;
        }

        public function getPostCode(): ?int
        {
            return $this->post_code;
        }

        public function setPostCode(?int $post_code): self
        {
            $this->post_code = $post_code;

            return $this;
        }

        public function getPhone(): ?int
        {
            return $this->phone;
        }

        public function setPhone(?int $phone): self
        {
            $this->phone = $phone;

            return $this;
        }

        public function getStreet(): ?string
        {
            return $this->street;
        }

        public function setStreet(string $street): self
        {
            $this->street = $street;

            return $this;
        }

        public function getDefaultShipAddress(): ?bool
        {
            return $this->defaultShipAddress;
        }

        public function setDefaultShipAddress(bool $defaultShipAddress): self
        {
            $this->defaultShipAddress = $defaultShipAddress;

            return $this;
        }


        public function getAdress(): string
        {
            return $this->adress;
        }


        public function setAdress($adress): self
        {
            $this->adress = $adress;

            return $this;
        }

    }
