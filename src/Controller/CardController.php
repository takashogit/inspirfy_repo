<?php


    namespace App\Controller;

    use App\Entity\{Order, UserAdress};
    use App\Repository\UserRepository;
    use Stripe\Charge;
    use Stripe\Error\Card;
    use Stripe\Stripe;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\{HttpFoundation\JsonResponse, HttpFoundation\Request};

    class CardController extends AbstractController
    {


        /**
         * @param Request $request
         * @param UserRepository $user_repository
         * @return JsonResponse
         */
        public function __invoke(Request $request, UserRepository $user_repository): JsonResponse
        {
            $requestData = $request->getContent();
            // user payment  hare
            $data = json_decode($requestData);
            $user = $user_repository->findOneBy(['email' => $data->order->userEmail]);
            // 0 is not auth user
            $user = ($user === null) ? 0 : $user->getId();
            $assdo = $data->order->orderlist;
            $order_id = strtoupper(substr(uniqid(sha1(time())), 0, 6));

            /*
             * Stripe
             */
            Stripe::setApiKey($_ENV['STRIPLE_SEKRET_KEY']);

            // Stripe expects prices in pennies
            $totalSum = $data->order->totalSum * 100;
            $token = $data->order->stripeToken;
            try {
                Charge::create([
                                   'amount'      => $totalSum,
                                   'currency'    => 'USD',
                                   'description' => 'Order id - '.$order_id,
                                   'source'      => $token,
                               ]);
            } catch (Card $e) {
                return new JsonResponse('Payment not accept', JsonResponse:: HTTP_NOT_ACCEPTABLE);
            } catch (\Exception $e) {
                return new JsonResponse('Payment impossible', JsonResponse:: HTTP_NOT_ACCEPTABLE);
            }


            // insert to db  user  Order  data
            // Check if shipping is select by user. 1 =true
            try {

                if ((bool)$data->order->shipping == 1) {
                    $userAdress = new UserAdress();
                    $userAdress->setUserId($user);
                    $userAdress->setCountry($data->order->country);
                    $userAdress->setState($data->order->state);
                    $userAdress->setCity($data->order->city);
                    $userAdress->setAdress($data->order->adressLine1);
                    $userAdress->setStreet($data->order->adressLine2);
                    $userAdress->setPhone($data->order->phone);
                    $userAdress->setPostCode($data->order->postalCode);
                    $userAdress->setDefaultShipAddress($data->order->defaultShipAddress);
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($userAdress);
                    $entityManager->flush();
                }


                foreach ($assdo as $orders) {
                    $order = new Order();
                    $order->setHashProduct($orders->product_hesh);
                    $order->setPrice((int)$orders->price);
                    $order->setQuantity((int)$orders->quantity);
                    $order->setUser($user);
                    $order->setOrderStatus(1);
                    $order->setOrderNum($order_id);
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($order);
                    $entityManager->flush();
                }

                return new JsonResponse('Saved, order accept', JsonResponse:: HTTP_ACCEPTED);

            } catch (\Exception $e) {

                return new JsonResponse('error, order not accept', JsonResponse:: HTTP_NOT_ACCEPTABLE);

            }
        }
    }