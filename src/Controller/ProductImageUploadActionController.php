<?php

namespace App\Controller;

use App\Entity\ProductImg;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

final class ProductImageUploadActionController
{
    public function __invoke(Request $request) :ProductImg
    {
        $uploadedImage = $request->files->get('file');
        if(!$uploadedImage) {
            throw  new BadRequestHttpException('"file" is required');
        }
        $mediaImg = new ProductImg();
        $mediaImg->file = $uploadedImage;
        return $mediaImg;
    }
}
