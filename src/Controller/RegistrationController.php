<?php

namespace App\Controller;


use App\Service\Translation\CyrillicToLatin;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\{Bundle\FrameworkBundle\Controller\AbstractController,
    Component\HttpFoundation\JsonResponse,
    Component\HttpFoundation\Request,
    Component\HttpFoundation\Response,
    Component\Security\Core\Encoder\UserPasswordEncoderInterface
};
use App\Repository\UserRepository;
use App\Service\Mailer\SendMailService;

class RegistrationController extends AbstractController
{
    /**
     * @var UserRepository $userRepository
     * @var UserPasswordEncoderInterface $usersRepository
     * @var SendMailService $sendMailService
     */
    private UserRepository $usersRepository;
    private UserPasswordEncoderInterface $passwordEncoder;
    private SendMailService $sendMailService;

    /**
     * AuthController Constructor
     *
     * @param UserRepository $usersRepository
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param SendMailService $sendMailService
     */
    public function __construct(UserRepository $usersRepository, UserPasswordEncoderInterface $passwordEncoder, SendMailService $sendMailService)
    {
        $this->usersRepository = $usersRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->sendMailService = $sendMailService;
    }

    /**
     * Register new user
     * @param Request $request
     * @param CyrillicToLatin $cyrillicToLatin
     * @return Response
     */
    public function register(Request $request,CyrillicToLatin $cyrillicToLatin)
    {
        $request_params = json_decode($request->getContent());

        try {
            $newUserData['email'] = $request_params->email;
            $newUserData['name'] = $cyrillicToLatin->cyrilToLat($request_params->name);
            $newUserData['password'] = $request_params->password;

               // $newUserData['avatar'] = $request_params->avatar;

            //TODO: add next fields from registration form

            $user = $this->usersRepository->createNewUser($newUserData);
        } catch (Exception $e) {
            return new JsonResponse('User with same email address exist ' , JsonResponse::HTTP_FORBIDDEN);
        }
        $password = $this->passwordEncoder->encodePassword($user, $request_params->password);
        try {
            $this->usersRepository->upgradePassword($user, $password);
        } catch (OptimisticLockException $e) {
        } catch (ORMException $e) {
        }

        $this->sendMailService->setRecipientEmail($request_params->email)
            ->setMessageSubject('Inspirfy Registration')
            ->setMessageBody('https://inspirfy.com Thank you for your registration!')
            ->send();

        return new JsonResponse(sprintf('User %s successfully created', $user->getUsername()));
    }




}
