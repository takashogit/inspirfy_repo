<?php

namespace App\Controller;

use App\Entity\ProductsFilters;
use App\Repository\ProductFilterRepository;
use Symfony\Component\HttpFoundation\Request;

final class ProductsFilterGetIdxController
{
    public function __invoke(Request $request, $hash_category): ProductsFilters
    {
        if (is_string($hash_category)) {
            $filter = (new ProductFilterRepository)->getFilters($hash_category);
        }

        return $filter;
    }
}