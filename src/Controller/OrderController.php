<?php


    namespace App\Controller;

    use App\Entity\Order;
    use App\Entity\UserAdress;
    use App\Repository\OrderRepository;
    use App\Repository\UserRepository;
    use App\Service\Delivery\DeliveryService;
    use EasyPost\Address;
    use EasyPost\EasyPost;
    use EasyPost\Shipment;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Routing\Annotation\Route;


    /**
     * Class OrderController
     * @package App\Controller
     */
    class OrderController extends AbstractController
    {

        /**
         *
         * @Route("/order" ,name="add_order", methods={"POST"})
         * @param Request $request
         * @param UserRepository $user_repository
         * @return JsonResponse
         */

        public function makeOrder(Request $request, UserRepository $user_repository)
        {

            $requestData = $request->getContent();
            $data = json_decode($requestData);
            $user = $user_repository->findOneBy(['email' => $data->order->userEmail]);
            // 0 is not auth user
            $user = ($user === null) ? 0 : $user->getId();

            $assdo = $data->order->orderlist;
            $order_id = strtoupper(substr(uniqid(sha1(time())), 0, 6));


            // ADD Order to database
            try {
                foreach ($assdo as $orders) {
                    $order = new Order();
                    $order->setHashProduct($orders->product_hesh);
                    $order->setPrice((int)$orders->price);
                    $order->setQuantity((int)$orders->quantity);
                    $order->setUser($user);
                    $order->setOrderStatus(1);
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($order);
                    $entityManager->flush();
                }

                return new JsonResponse('Saved, order created', JsonResponse:: HTTP_ACCEPTED);

            } catch (\Exception $e) {

                return new JsonResponse('error, order not accept', JsonResponse:: HTTP_NOT_ACCEPTABLE);

            }

        }

        /**
         * @Route("/order/delivery", name="delivery", methods={"POST"})
         * @param Request $request
         */
        public function delivery(Request $request): JsonResponse
        {

            $requestData = $request->getContent();
            $data = json_decode($requestData);
            EasyPost::setApiKey($_ENV['EASY_POST']);
            $delivery_service = new DeliveryService();
            $to_address = Address::create($delivery_service->to_address($data->adress));
            $from_address = Address::create($delivery_service->from_address());
            $parcel_params = $delivery_service->parcel_params();
            $customs_info = $delivery_service->customs_info();
            $shipment = Shipment::create([
                                             "to_address"   => $to_address,
                                             "from_address" => $from_address,
                                             "parcel"       => $parcel_params,
                                             "customs_info" => $customs_info,
                                         ]);

            return $this->json($shipment->rates, JsonResponse::HTTP_OK);
            // dd($parcel_params);$shipment->rates[0]
        }


        /**
         *
         * @Route("/user/{id}", methods={"GET"})
         * @param int $id
         * @param OrderRepository $order_repository
         * @return JsonResponse
         */

        public function getUserOrder(int $id, OrderRepository $order_repository)
        {

            $requestData = $order_repository->selectUserOrder($id);
            return new JsonResponse($requestData, JsonResponse::HTTP_OK);

        }


    }