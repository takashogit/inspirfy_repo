<?php /** @noinspection PhpInconsistentReturnPointsInspection */

namespace App\Controller;

    use App\Entity\ProductsFilters;
    use App\Repository\{CategoryRepository, ProductFilterRepository};
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\HttpFoundation\{JsonResponse, Request};

    /**
     * Class CategoryController
     * @package App\Controller
     */
    class CategoryController extends AbstractController
    {

        public function getCategory(Request $request, $hash = '', CategoryRepository $categoryRepository): JsonResponse
        {
            $parent = '';
            $requestParams = $request->getContent();

            if ($requestParams == '' && $hash == '') {

                $response = $categoryRepository->getRootCategories();

            } else {

                $rq = json_decode($requestParams);
                if ($hash !== '') {
                    $parent = $hash;
                } elseif ($rq->category !== '') {
                    $parent = $rq->category;
                   // $parent = 'df';
                }
                $response = $categoryRepository->getSubcategories($parent);
            }

            return new JsonResponse(
                $response
            );
        }

        public function getFilters(Request $request, $hash = '', ProductFilterRepository $filterRepository)
        {
            if ($hash !== '') {
                $filter = $filterRepository->getFilters($hash);

                return new JsonResponse(
                    $filter
                );
            }
        }
    }

