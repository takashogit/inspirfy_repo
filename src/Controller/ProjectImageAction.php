<?php
    declare(strict_types=1);

    namespace App\Controller;

    use App\Entity\ProjectsArchitects;
    use Liip\ImagineBundle\Binary\BinaryInterface;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\DependencyInjection\ContainerInterface;
    use App\Entity\Project;
    use App\Form\ImageTypeProject;
    use ApiPlatform\{Core\Validator\Exception\ValidationException,
        Core\Validator\ValidatorInterface
    };
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\{HttpFoundation\Request,
        Form\FormFactoryInterface,
        HttpKernel\Exception\BadRequestHttpException,
        Security\Core\Authentication\Token\TokenInterface,
        Security\Core\User\UserInterface};

    class ProjectImageAction
    {

        private FormFactoryInterface $formFactory;
        private EntityManagerInterface $entityManager;
        private ValidatorInterface $validator;
        private  ContainerInterface $container;

        /**
         * constructor.
         * @param FormFactoryInterface $formFactory
         * @param EntityManagerInterface $entityManager
         * @param ValidatorInterface $validator
         * @param ContainerInterface $container
         */
        public function __construct(
            FormFactoryInterface $formFactory,
            EntityManagerInterface $entityManager,
            ValidatorInterface $validator,
            ContainerInterface $container
        ) {
            $this->formFactory = $formFactory;
            $this->entityManager = $entityManager;
            $this->validator = $validator;
            $this->container = $container;
        }

        /**
         * POST METHOD
         * @param Request $request
         * @return Project
         */
        public function __invoke(Request $request)
        {
            //get params from form-data request
            $res = $request->request;
            $project = new Project();
            $archProject = new ProjectsArchitects();
            $form = $this->formFactory->create(ImageTypeProject::class, $project);
            $form->handleRequest($request);

            if ($form->isSubmitted()) {

                $project->setName($res->get('name'));
                $project->setZipcode($res->get('zipcode'));
                $project->setStyleId($res->get('styleId'));
                $project->setCost($res->get('cost'));
                $project->setProjectStyle($res->get('projectStyle'));
                $project->setTags($res->get('tags'));
                $project->getProjectDescription($res->get('projectDescription'));
                $project->setAdrCountriesId($res->get('adrCountriesId'));
                $project->setAdrCitiesId($res->get('adrCitiesId'));
                $project->setPoster($res->get('regId'));
                $this->entityManager->persist($project);
                $this->entityManager->flush();
                //add project and  architect id
                $archProject->setProjetIdProjet($project->getId());
                $archProject->setArchitectIdArchitect($res->get('user_id'));
                $this->entityManager->persist($archProject);
                $this->entityManager->flush();

                $this->makeThumbNail($project);
                 $project->setFile(null);

                return $project;
            }
            throw new ValidationException($this->validator->validate($project));

        }

        protected function makeThumbNail(Project $project)
        {
            $filters= ['thumbnail_225'=>'images_225_filesystem'];

            foreach ($filters as $filter=>$bucket)
            {
                $array = explode('/', $project->getPoster());
                $link = end($array);
                $poster = $this->container->get('liip_imagine.data.manager')->find($filter,$project->getPoster());
                $poster= $this->container->get('liip_imagine.filter.manager')->applyFilter($poster,$filter);
                $this->container->get('oneup_flysystem.mount_manager')->getFilesystem($bucket)->write($link,$poster->getContent());
            }

        }


    }




