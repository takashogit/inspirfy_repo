<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\FileUploader\AvatarFileUploader;
use App\Service\Translation\CyrillicToLatin;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use App\Repository\{OrderRepository, UserAdressRepository, UserRepository};
use App\Service\ColorFounder\ColorFounder;
use App\Service\Payment\Payment;
use App\Service\Mailer\SendMailService;
use League\ColorExtractor\{Palette, Color, ColorExtractor};
use Symfony\{Bundle\FrameworkBundle\Controller\AbstractController,
    Component\HttpFoundation\File\UploadedFile,
    Component\HttpFoundation\JsonResponse,
    Component\HttpFoundation\Request,
    Component\HttpFoundation\Response,
    Component\Routing\Annotation\Route,
    Component\Security\Core\Encoder\UserPasswordEncoderInterface};
use Vich\UploaderBundle\Handler\DownloadHandler;


/**
 * Class ProfileController
 * @package App\Controller
 *
 */
class ProfileController extends AbstractController
{
    const RECOVERY_URL = 'https://inspirfy.com/recovery?code=';
    const ENCRYPTION_KEY = 'ab86d144e3f080b61c7c2e43';
    const AES = 'AES-128-CBC';

    private $arr = [1, 3, 9, 27, 81];

    /**
     * @Route("api/v1/profile/get_current_user", name="profile")
     * @param OrderRepository $order_repository
     * @param UserAdressRepository $address_repository
     * @return JsonResponse
     */
    public function getCurrentUser(OrderRepository $order_repository, UserAdressRepository $address_repository)
    {
        $user = $this->getUser();
        if (!$user) {
            return new JsonResponse(['error' => 'Not logged in'], JsonResponse::HTTP_FORBIDDEN);
        }

        $user_orders = $order_repository->selectUserOrder($user->getId());
        $userAddresses = $address_repository->getUserAddresses($user->getId());


        // @todo
        $cards_s = new Payment('5132 1457 5706 9516', '318', '08/20');
        $user_cards = $cards_s->creditCard();

        $orders = [];
        foreach ($user_orders as $k => $v) {
            $orders[$v['order_num']][$k + 1] = $v;
        }

        //return user cabinet info
        $data = [
            'id' => $user->getId(),  //user id
            'name' => $user->getName(),
            'email' => $user->getUsername(),
            'role' => $user->getRoles(),
            'orders' => $orders, //user list orders
            'avatar' =>   $user->getAvatar(),
            'pay_card' => $user_cards, //user credit cards
            'address' => $userAddresses, //user saved address
        ];

        return new JsonResponse(['data' => $data, JsonResponse::HTTP_OK]);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function editName(Request $request)
    {
        $current_user_id = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($current_user_id);
        $request_params = json_decode($request->getContent());

        if (!empty($request_params->new_name)) {
            $user->setName($request_params->new_name);
            $em->flush();
        }

        return new JsonResponse(
            [
                'status' => 'success',
                'message' => $request_params->new_name,
            ]
        );
    }

    /**
     * @param Request $request
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function changePassword(
        Request $request,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $userPasswordEncoder
    )
    {
        $current_user = $this->getUser();
        $request_params = json_decode($request->getContent());
        $userRepository->upgradePassword($current_user,
            $userPasswordEncoder->encodePassword($current_user,
                $request_params->new_pass));
        return new JsonResponse(['status' => 'success',
            'message' => 'Password successfully changed',]);

    }

    /**
     * Upload user avatar
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function addPhoto(Request $request)
    {

        $avatar = $request->files->get('file');
        $googleAvatar = $request->get('google');
        $userId = $this->getUser()->getId();
        $imagePath = $this->getUploadDir();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($userId);
        if($googleAvatar)
        {
            $user->setAvatar($googleAvatar);
            $em->persist($user);
            $em->flush();
            return  new JsonResponse('Image uploded',Response::HTTP_OK);
        }
        if($avatar){
            $fileName = $this->uniqFilename().'.jpg';
            $avatar->move($this->getUploadDir(),$fileName);


            $user->setAvatar('https://inspirfy.com/static/avatars/'.$fileName);
            $em->persist($user);
            $em->flush();
            return  new JsonResponse('Image uploded',Response::HTTP_OK);
        }
        return new JsonResponse('Something went wrong',Response::HTTP_BAD_REQUEST);
    }


    private function uniqFilename()
    {
        return md5(uniqid());
    }

    private function  getUploadDir()
    {
        return $this->getParameter('upload_avatars');
    }



    /**
     * @deprecated
     * @Route("/profile/pay", name="core")
     *
     */
    public function pay()
    {

        $s = 'W 22" / D 50" / H 24" / 68 lb.';
        $f = explode('/', $s);
        $arr = [];
        foreach ($f as $k => $v) {

            if (!next($f)) {
                $arr[substr(trim($v), 2, 3)] = substr(trim($v), 0, 2);
            } else {
                // делаем что-либо с каждым элементом
                $arr[substr(trim($v), 0, 2)] = substr(trim($v), 2, 4);
            }
        }

        /*
         * get list  product images
         * test hesh product
         */
        $hash = '0015245fecaa109611ee537d6e67bbc4';
        $hash2 = '0000d96ec6ee3a1d658740b4dc30bcd3';


        if ($hash2) {
            //  nothing
        }
        $entityManager = $this->getDoctrine()->getManager();
        $query = $entityManager->createQuery(
            'SELECT p.hashProduct,p.hashImage,p.linkImage
            FROM App\Entity\ProductImg p
            WHERE p.hashProduct= :hash'
        )->setParameter('hash', $hash);

        // returns an array of Product objects
        $res = $query->getResult();


        return new JsonResponse([$res]);

    }



    /**
     * Test  image  colors
     * @Route("/profile/test", name="pay")
     *
     */
    public function tester()
    {

        $file_path = $this->getParameter('kernel.project_dir') . '/public/img/joker.jpg';
        $palette = Palette::fromFilename($file_path);
        $extractor = new ColorExtractor($palette);
        $colors = $extractor->extract(5);
        foreach ($colors as $n => $col) {
            if ($n < 4) {
                $colorHex[] = Color::fromIntToHex($col);
            }
        }
        $colorFoudner = new ColorFounder();
        $rgbColors = $colorFoudner->getName($colorHex[0]);

        foreach ($colorHex as $color) {
            echo "<div style='background-color: " . $color . ";'> " . $color . "<div>";
        }

        dd($rgbColors);


    }

    /**
     * Return Fake values for Char*
     * @Route("api/v1/profile/grafic", name="grafic")
     *
     */
    public function grafic(): JsonResponse
    {
        $result = array();
        $datetime = \DateTime::createFromFormat('d/m/Y', "08/09/2020");
        $interval = new \DateInterval('P1D');
        if ($datetime->format('D') !== 'Mon') $datetime->modify('last monday');
        $week = new \DatePeriod($datetime, $interval, 10);
        foreach ($week as $day) {
            $result[] = [$day->format('M d'), rand(50, 30900)];
        }

        return new JsonResponse($result, Response::HTTP_OK);
    }


    /**
     * Update user role to Architect
     *
     * @Route("api/v1/profile/role", name="role", methods={"POST"})
     * @param UserRepository $userRepository
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function userRole(UserRepository $userRepository)
    {

        if ($this->getUser()) {
            $userRepository->updateUserRoleToArchitect($this->getUser());
        }
        return new JsonResponse('User role change', Response::HTTP_OK);
    }



    /*
     * Descriptiomn function  for MLM
     */


    /**
     * @param $plaintext
     * @return string
     */
    public function encrypt($plaintext)
    {
        // Encrypt
        $ivlen = openssl_cipher_iv_length(self::AES);
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($plaintext, self::AES, self::ENCRYPTION_KEY, $options = OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, self::ENCRYPTION_KEY, $as_binary = true);
        $ciphertext = base64_encode($iv . $hmac . $ciphertext_raw);
        return $ciphertext;

    }

    /**
     * @param $ciphertext
     * @return false|string
     */
    public function decrypt($ciphertext)
    {

        // Decrypt
        $c = base64_decode($ciphertext);
        $ivlen = openssl_cipher_iv_length(self::AES);
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len = 32);
        $ciphertext_raw = substr($c, $ivlen + $sha2len);
        $plaintext = openssl_decrypt($ciphertext_raw, self::AES, self::ENCRYPTION_KEY, $options = OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, self::ENCRYPTION_KEY, $as_binary = true);
        if (hash_equals($hmac, $calcmac))
            return $plaintext;
    }

    //Get user e-mail
    public function forgotPassword(Request $request, UserRepository $userRepository, SendMailService $sendMailService)
    {
        $requestParams = json_decode($request->getContent());
        $email = $requestParams->email;

        $user = $userRepository->findOneBy(['email' => $email]);

        if(!$user){
            return new JsonResponse('User with this e-mail does not exist', JsonResponse::HTTP_UNAUTHORIZED);
        }

        $recovery_url = self::RECOVERY_URL
            . $this->encrypt(json_encode(array(
                'email' => $user->getEmail(),
                'password' => $user->getPassword()
            ))). '&email=' . $user->getEmail();

        $sendMailService->setRecipientEmail($user->getEmail())
            ->setMessageSubject('Inspirify Password Recovery')
            ->setMessageBody('Please, visit this link to set new password: '.$recovery_url)
            ->send();

        return new JsonResponse('Ok', Response::HTTP_OK);
    }

    //Get new password and update password in DB
    public function forgotPasswordConfirm(
        Request $request,
        UserRepository $userRepository,
        SendMailService $sendMailService,
        UserPasswordEncoderInterface $passwordEncoder)
    {
        $requestParams = json_decode($request->getContent());
        $requestParams->code = str_replace('%20', '+', $requestParams->code);
        $data = json_decode($this->decrypt($requestParams->code), true);

        if(is_null($data)) return new Response('data is null');
        if(!is_array($data)) return new Response('data is not array');

        if (count($data) != 2){
            return new Response('Wrong data ...');
        }

        $user = $userRepository->findOneBy(['email' => $data['email']]);
        if(!$user){
            return new JsonResponse('Code is wrong', JsonResponse::HTTP_UNAUTHORIZED);
        }

        if($data['password'] != $user->getPassword()){
            return new JsonResponse('Code is wrong', JsonResponse::HTTP_UNAUTHORIZED);
        }

        $password = $passwordEncoder->encodePassword($user, $requestParams->password);
        try {
            $userRepository->upgradePassword($user, $password);
        } catch (OptimisticLockException $e) {
        } catch (ORMException $e) {
        }

        return new Response('Ok', Response::HTTP_OK);
    }
}
