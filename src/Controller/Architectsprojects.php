<?php

declare(strict_types=1);
namespace App\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ProjectsArchitectsRepository;
use Symfony\Component\HttpFoundation\Response;


class Architectsprojects
{

    public function __invoke(ProjectsArchitectsRepository $projectsArchitectsRepository)
    {

        $archProjects = $projectsArchitectsRepository->selectArchitectorWithProjects();




        foreach ($archProjects as $k=>$project)
        {
            $data[$project['userName']][]= array_merge($project,$projectsArchitectsRepository->countImagesInProject($project['projetIdProjet']));
        }

        return new JsonResponse($data,Response::HTTP_OK);

    }





}