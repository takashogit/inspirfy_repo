<?php
    declare(strict_types=1);

    namespace App\Controller;

    use App\Entity\Project;
    use ApiPlatform\{Core\Validator\Exception\ValidationException,
                     Core\Validator\ValidatorInterface};
    use Symfony\Component\DependencyInjection\ContainerInterface;
    use App\Entity\ProjectImages;
    use App\Form\ImageType;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\{HttpFoundation\Request,
        Form\FormFactoryInterface,
        HttpKernel\Exception\BadRequestHttpException};
    use Liip\ImagineBundle\Binary\BinaryInterface;

    class UploadImageAction
    {

        private FormFactoryInterface $formFactory;
        private EntityManagerInterface $entityManager;
        private ValidatorInterface $validator;
        private  ContainerInterface $container;

        /**
         * UploadImageAction constructor.
         * @param FormFactoryInterface $formFactory
         * @param EntityManagerInterface $entityManager
         * @param ValidatorInterface $validator
         * @param ContainerInterface $container
         */
        public function __construct(
            FormFactoryInterface $formFactory,
            EntityManagerInterface $entityManager,
            ValidatorInterface $validator,
            ContainerInterface $container)
        {
            $this->formFactory = $formFactory;
            $this->entityManager = $entityManager;
            $this->validator = $validator;
            $this->container = $container;
        }

        /**
         * POST METHOD
         * @param Request $request
         * @return ProjectImages
         */
        public function __invoke(Request $request)
        {


            //get params from form-data request
            $res = $request->request;
            $style_id = $res->get('style_id');

            $image = new ProjectImages();

            $form = $this->formFactory->create(ImageType::class, $image);
            $form->handleRequest($request);

            if ($form->isSubmitted()) {

                $image->setProjectId((int)$res->get('project_id'));
                $image->setTitle((string)$res->get('title'));
                $image->setCategoryId((string)$res->get('category_id'));
                $image->setStyleId((int)$style_id);
                $this->entityManager->persist($image);
                $this->entityManager->flush();
                $this->makeThumbNail($image);
                $image->setFile(null);
                return $image;
            }
            throw new ValidationException($this->validator->validate($image));

        }

        protected function makeThumbNail(ProjectImages $image ):void
        {
            $filters= ['thumbnail_225'=>'images_225_filesystem'];
            foreach ($filters as $filter=>$bucket)
            {
                $array = explode('/', $image->getLinkImage());
                $link = end($array);
                $image = $this->container->get('liip_imagine.data.manager')->find($filter,$image->getLinkImage());
                $image= $this->container->get('liip_imagine.filter.manager')->applyFilter($image,$filter);
                $this->container->get('oneup_flysystem.mount_manager')->getFilesystem($bucket)->write($link,$image->getContent());
            }


        }

    }