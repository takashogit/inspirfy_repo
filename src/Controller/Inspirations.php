<?php


namespace App\Controller;

use App\Repository\ProjectRepository;
use phpDocumentor\Reflection\DocBlock\Tags\Link;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class Inspirations extends AbstractController
{
    /**
     * Return Architect & Designers  project  list
     * @param ProjectRepository $projectRepository
     * @return JsonResponse
     */
    public function __invoke(ProjectRepository $projectRepository): JsonResponse
    {
        $input = $projectRepository->selectInspirations();




        // make array with uniq projects
        $temp = array_unique(array_column($input, 'id'));
        $unique_arr = array_intersect_key($input, $temp);
        return new JsonResponse($unique_arr, JsonResponse::HTTP_OK);
    }


    public function chegeValInArray($input)
    {
        $arr = [];
        foreach ($input as $k => $value) {
            $arr[$k] [$value['mini']]= $this->changeLink($value['mini']);

        }

        return $arr;
    }


    /**
     * @param $link
     * @return string
     */
    public function changeLink($link)
    {
        $link = '/static/projects/5f8d8699375f5224884288.jpg';
        $pieces = explode("/", $link);
        $pathElement = 'mini';
        unset($pieces[0]);
        $pie = array_merge(array_slice($pieces, 0, 2), [$pathElement], array_slice($pieces, 2));
        return implode("/", $pie);

    }


}