<?php


namespace App\Service\PayPal;

use PayPal\Api\{Payer, Item, ItemList, Details, Amount, Transaction, RedirectUrls, Payment};
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;


/**
 * Class PayPalService
 * @package App\Service\PayPal
 */
class PayPalService implements PayPalServiceInterface
{
    private $order;

    /**
     * PayPalService constructor.
     * @param $bascket
     */
    public function __construct($bascket)
    {
        $this->order = $bascket;

    }

    /**
     * @return string|null
     */
    public function createPayment(): ?string
    {
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        foreach ($this->session->get('cart') as $product) {
            $items[] = $item = new Item();
            $item->setName($product['name'])
                ->setCurrency('USD')
                ->setQuantity($product['amount'])
                ->setPrice($product['price']);
        }

        $itemList = new ItemList();
        $itemList->setItems($items);

        $details = new Details();
        $details->setShipping(0)
            ->setTax(0)
            ->setSubtotal($this->order->get('total_price'));

        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($this->session->get('cart_data/total_price'))
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($this->router->generate('execute-payment', [], UrlGeneratorInterface::ABSOLUTE_URL))
            ->setCancelUrl($this->router->generate('cart', [], UrlGeneratorInterface::ABSOLUTE_URL));

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);

        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $_ENV['PAYPAL_CLIENT_ID'],     // ClientID
                $_ENV['PAYPAL_CLIENT_SECRET']     // ClientSecret
            )
        );

        $payment->create($apiContext);
        return $payment->getApprovalLink();


    }
}