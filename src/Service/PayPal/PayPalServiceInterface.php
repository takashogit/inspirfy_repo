<?php

    namespace App\Service\PayPal;


    /**
     * Class PayPalService
     * @package App\Service\PayPal
     */
    interface PayPalServiceInterface
    {
        /**
         * @return string|null
         */
        public function createPayment();
    }