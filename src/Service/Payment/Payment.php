<?php


    namespace App\Service\Payment;


    /**
     * Class Payment
     * @package App\Service\Payment
     */
    class Payment
    {
        private $cardNumber;
        private $cardDate;
        private $cvv;

        /**
         * Payment constructor.
         * @param $cardNumber
         * @param $cardDate
         * @param $cvv
         */
        public function __construct($cardNumber, $cardDate, $cvv)
        {
            $this->cardNumber = $cardNumber;
            $this->cardDate = $cardDate;
            $this->cvv = $cvv;
        }

        /**
         * @return array
         */
        public function creditCard()
        {
            return [
                'number' => $this->getCardNumber(),
                'cvv:'   => $this->getCvv(),
                'exp:'   => $this->getCardDate()];
        }

        /**
         * @return mixed
         */
        public function getCardNumber()
        {
            return $this->cardNumber;
        }

        /**
         * @param mixed $cardNumber
         */
        public function setCardNumber($cardNumber): void
        {
            $this->cardNumber = $cardNumber;
        }

        /**
         * @return mixed
         */
        public function getCardDate()
        {
            return $this->cardDate;
        }

        /**
         * @param mixed $cardDate
         */
        public function setCardDate($cardDate): void
        {
            $this->cardDate = $cardDate;
        }

        /**
         * @return mixed
         */
        public function getCvv()
        {
            return $this->cvv;
        }

        /**
         * @param mixed $cvv
         */
        public function setCvv($cvv): void
        {
            $this->cvv = $cvv;
        }


    }