<?php


namespace App\Service\Translation;


/**
 * Class CyrillicToLatin
 * @package App\Service\Translation
 */
class CyrillicToLatin
{


    /**
     * @param $textCyr
     * @return string|string[]
     */
    public function cyrilToLat($textCyr)
    {
        // Check  if string contains  cyrillic  character
        return (bool)preg_match('/[\p{Cyrillic}]/u', $textCyr) ?
                str_replace($this->Cyr(), $this->Lat(), $textCyr) : $textCyr;

    }

    /**
     * @return string[]
     */
    private function Lat(): array
    {
        return [
            'a', 'b', 'v', 'g', 'd', 'e', 'io', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
            'r', 's', 't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'sht', 'a', 'i', 'y', 'e', 'yu', 'ya',
            'A', 'B', 'V', 'G', 'D', 'E', 'Io', 'Zh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P',
            'R', 'S', 'T', 'U', 'F', 'H', 'Ts', 'Ch', 'Sh', 'Sht', 'A', 'I', 'Y', 'e', 'Yu', 'Ya'
        ];
    }

    /**
     * cyrillic  character
     * @return string[]
     */
    private function Cyr(): array
    {
        return [
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п',
            'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я',
            'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П',
            'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'
        ];

    }


    /**
     * check if sting is Cyrillic
     * @param $text
     * @return false|int
     */

    public function isCyrillic($text)
    {
        return preg_match('/[А-Яа-яЁё]/u', $text);
    }


}