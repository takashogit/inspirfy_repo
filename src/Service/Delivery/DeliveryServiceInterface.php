<?php

    namespace App\Service\Delivery;

    interface DeliveryServiceInterface
    {
        /**
         * @param $tol
         * @return mixed
         */
        public function to_address($tol);

        /**
         * @return mixed
         */
        public function from_address(): array;

        /**
         * @return array
         */
        public function parcel_params();

        /**
         * @return mixed
         */
        public function customs_info();
    }