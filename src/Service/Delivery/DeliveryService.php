<?php


    namespace App\Service\Delivery;

    class DeliveryService implements DeliveryServiceInterface
    {

        /**
         * @param $tol
         * @return mixed
         */
        public function to_address($tol)
        {
            return [
                "name"    => $tol->name,
                "street1" => $tol->street1,
                "street2" => $tol->street2,
                "city"    => $tol->city,
                "zip"     => $tol->zip,
             //   "country" => $tol->country,
                // "email"   => $tol["email"],
            ];

        }


        /**
         * Address  by default
         * @return mixed
         */
        public function from_address(): array
        {
            return [
                "company" => "inspirfy",
                "street1" => "388 Townsend St",
                "street2" => "Apt 20",
                "city"    => "San Francisco",
                "state"   => "CA",
                "country" => "US",
                "zip"     => "94107",
                "phone"   => "323-855-0394",
            ];
        }


        /**
         * @return array
         */
        public function parcel_params()
        {
            return [
                "length"             => 3,
                "width"              => 5.9,
                "height"             => 5,
                "predefined_package" => null,
                "weight"             => $this->_lbtooz(2.8),
            ];

        }

        /**
         * @return mixed
         */
        public function customs_info()
        {
            return [];
        }


        /**
         * @param $lb
         * @return float
         */
        private function _lbtooz($lb): float
        {
            return $lb * 16;
        }


    }