<?php

namespace App\Service\ColorFounder;


/**
 * Class ColorFounder
 * @package App\Service\ColorFounder
 */
class ColorFounder
{


    /**
     * Basic color list
     * @var array
     *
     */

    protected $baseColors = [
        'ee0202' => 'red',
        'e16c5a' => 'red',
        'fbef00' => 'yellow',
        '1e15f6' => 'blue',
        '0080FF' => 'blue',
        'f47200' => 'orange',
        '2e9102' => 'green',
        '00FF00' => 'green',
        '9e0bf6' => 'purple',
        '8000FF' => 'purple',
        'ffffff' => 'white',
        '000000' => 'black',
        'd20797' => 'pink',
        'ff00f6' => 'pink',
        '9a0005' => 'brown',
        '808080' => 'gray',
    ];

    /**
     * ColorFounder constructor.
     * @param bool $baseColors
     */
    public function __construct($baseColors = false)
    {
        if ($baseColors && is_array($baseColors)) {
            $this->baseColors = $baseColors;
        }
    }

    /**
     * Convert A HEX Code to an array of RGB values
     * @param $color
     * @return array|bool
     */
    public function hexToRgb($color)
    {
        if ($color != "") {
            $color = ltrim($color, '#');
            list($r, $g, $b) = array_map('hexdec', str_split($color, 2));
            return array(
                'r' => $r,
                'g' => $g,
                'b' => $b
            );
        }
        return false;

    }

    /**
     * Calculate distance between the 2 colors
     *
     * @param array $color1 RGB values of Color 1
     * @param $color2
     * @return int Distance of the 2 RGB values
     */
    public function distTwoColors($color1, $color2)
    {
        if (is_array($color1) && is_array($color2)) {
            $delta_r = $color1['r'] - $color2['r'];
            $delta_g = $color1['g'] - $color2['g'];
            $delta_b = $color1['b'] - $color2['b'];
            return $delta_r * $delta_r + $delta_g * $delta_g + $delta_b * $delta_b;
        } else {
            return false;
        }

    }

    /**
     * Get the color name from the pre-defined base colors
     *
     * @param $color
     * @param bool $baseColors Base Colors to check from
     * @return array|string Color name or False
     */
    public function getName($color, $baseColors = false)
    {

        $dist_arr = [];
        $nearest = false;
        if ($baseColors && is_array($baseColors)) {
            $this->baseColors = $baseColors;
        }
        foreach ($this->baseColors as $k => $v) {
            $dist = $this->distTwoColors($this->hexToRgb($color), $this->hexToRgb($k));
            if ($nearest === false) {
                $nearest = $dist;
            } else {
                if ($nearest > $dist) {
                    $nearest = $dist;
                }
            }
            $dist_arr[$dist] = $v;
        }
        if (isset($dist_arr[$nearest])) {
            return $dist_arr[$nearest];
        } else {
            return false;
        }
    }


}