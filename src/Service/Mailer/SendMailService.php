<?php

namespace App\Service\Mailer;
use Swift_Message;

class SendMailService
{
    const SENDER_EMAIL = "noreply@inspirify.com";

    private $recipientEmail;
    private $messageSubject;
    private $messageBody;
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function getRecipientEmail()
    {
        return $this->recipientEmail;
    }

    public function setRecipientEmail($recipientEmail): self
    {
        $this->recipientEmail = $recipientEmail;
        return $this;
    }

    public function getMessageSubject()
    {
        return $this->messageSubject;
    }

    public function setMessageSubject($messageSubject): self
    {
        $this->messageSubject = $messageSubject;
        return $this;
    }

    public function getMessageBody()
    {
        return $this->messageBody;
    }

    public function setMessageBody($messageBody): self
    {
        $this->messageBody = $messageBody;
        return $this;
    }

    public function send(): void
    {
        $message = (new Swift_Message($this->getMessageSubject()))
            ->setFrom(self::SENDER_EMAIL)
            ->setTo($this->getRecipientEmail())
            ->setBody($this->getMessageBody())
        ;

        $this->mailer->send($message);
    }
}
