<?php


namespace App\Repository;

use App\Entity\ProductsFilters;
use App\Entity\ProductsSpecification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ProductFilterRepository
 * @package App\Repository
 */
class ProductFilterRepository extends ServiceEntityRepository
{

    /**
     * ProductFilterRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductsSpecification::class);
    }

    /**
     * @param string $category_hash
     * @return ProductsFilters
     */

    public function getFilters(string $category_hash)
    {
        $em = $this->getEntityManager();
        $filter = new ProductsFilters();
        $filter->materials = $em->createQuery(
            'SELECT 
                DISTINCT(spec.materials)
             FROM App\Entity\ProductsSpecification spec
             LEFT JOIN App\Entity\ProductsInCategory incat
                with incat.hashProduct = spec.hashProduct
                WHERE incat.hashCategory = :cat
                and length(spec.materials)>1'
        )
            ->setParameter('cat', $category_hash)
            ->getResult();
        foreach ($filter->materials as $key => $row) {
            $filter->materials[$key] = $row[1];
        }

        $filter->manufactured = $em->createQuery(
            'SELECT 
                DISTINCT(spec.manufactured)
                FROM App\Entity\ProductsSpecification spec
                left join App\Entity\ProductsInCategory incat
                with spec.hashProduct = incat.hashProduct
                where incat.hashCategory = :category
                and length(spec.manufactured)>1'
        )->setParameter('category', $category_hash)->getResult();
        foreach ($filter->manufactured as $key => $row) {
            $filter->manufactured[$key] = $row[1];
        }

        $filter->sold = $em->createQuery(
            'SELECT 
                DISTINCT(spec.sold)
                FROM App\Entity\ProductsSpecification spec
                left join App\Entity\ProductsInCategory incat
                with spec.hashProduct = incat.hashProduct
                where incat.hashCategory = :category
                and length(spec.sold)>1'
        )->setParameter('category', $category_hash)->getResult();

        foreach ($filter->sold as $key => $row) {
            $filter->sold[$key] = $row[1];
        }

        $filter->assembly_required = $em->createQuery(
            'SELECT 
                DISTINCT(spec.assembly_required)
                FROM App\Entity\ProductsSpecification spec
                left join App\Entity\ProductsInCategory incat
                with spec.hashProduct = incat.hashProduct
                where incat.hashCategory = :category
                and length(spec.assembly_required)>1'
        )->setParameter('category', $category_hash)->getResult();

        foreach ($filter->assembly_required as $key => $row) {
            $filter->assembly_required[$key] = $row[1];
        }

        $filter->style = $em->createQuery(
            'SELECT 
                DISTINCT(spec.style)
                FROM App\Entity\ProductsSpecification spec
                left join App\Entity\ProductsInCategory incat
                with spec.hashProduct = incat.hashProduct
                where incat.hashCategory = :category
                and length(spec.style)>1'
        )->setParameter('category', $category_hash)->getResult();

        foreach ($filter->style as $key => $row) {
            $filter->style[$key] = $row[1];
        }

        $filter->collection = $em->createQuery(
            'SELECT 
                DISTINCT(spec.collection)
                FROM App\Entity\ProductsSpecification spec
                left join App\Entity\ProductsInCategory incat
                with spec.hashProduct = incat.hashProduct
                where incat.hashCategory = :category
                and length(spec.collection)>1'
        )->setParameter('category', $category_hash)->getResult();

        foreach ($filter->collection as $key => $row) {
            $filter->collection[$key] = $row[1];
        }

        $filter->color = $em->createQuery(
            'SELECT 
                DISTINCT(spec.color)
                FROM App\Entity\ProductsSpecification spec
                left join App\Entity\ProductsInCategory incat
                with spec.hashProduct = incat.hashProduct
                where incat.hashCategory = :category
                and length(spec.color)>1'
        )->setParameter('category', $category_hash)->getResult();

        foreach ($filter->color as $key => $row) {
            $filter->color[$key] = $row[1];
        }

        $filter->commercial_grade = $em->createQuery(
            'SELECT 
                DISTINCT(spec.commercial_grade)
                FROM App\Entity\ProductsSpecification spec
                left join App\Entity\ProductsInCategory incat
                with spec.hashProduct = incat.hashProduct
                where incat.hashCategory = :category
                and length(spec.commercial_grade)>1'
        )->setParameter('category', $category_hash)->getResult();

        foreach ($filter->commercial_grade as $key => $row) {
            $filter->commercial_grade[$key] = $row[1];
        }

        return $filter;
    }
}