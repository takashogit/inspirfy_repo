<?php

namespace App\Repository;

use App\Entity\CategoryArch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategoryArch|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoryArch|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoryArch[]    findAll()
 * @method CategoryArch[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryArchRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategoryArch::class);
    }

    // /**
    //  * @return CategoryArch[] Returns an array of CategoryArch objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategoryArch
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
