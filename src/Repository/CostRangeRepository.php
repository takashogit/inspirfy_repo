<?php

namespace App\Repository;

use App\Entity\CostRange;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CostRange|null find($id, $lockMode = null, $lockVersion = null)
 * @method CostRange|null findOneBy(array $criteria, array $orderBy = null)
 * @method CostRange[]    findAll()
 * @method CostRange[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CostRangeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CostRange::class);
    }

    // /**
    //  * @return CostRange[] Returns an array of CostRange objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CostRange
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
