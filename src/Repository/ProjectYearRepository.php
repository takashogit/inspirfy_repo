<?php

namespace App\Repository;

use App\Entity\ProjectYear;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProjectYear|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProjectYear|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProjectYear[]    findAll()
 * @method ProjectYear[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectYearRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProjectYear::class);
    }

    // /**
    //  * @return ProjectYear[] Returns an array of ProjectYear objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProjectYear
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
