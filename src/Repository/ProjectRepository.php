<?php

namespace App\Repository;

use App\Controller\Architectsprojects;
use App\Entity\ProjectsArchitects;
use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }


    /**
     *  "REPLACE(danger_level, '_', ' ')"
     * select  inspiration
     * @return int|mixed|string
     */
    public function selectInspirations()
    {

        //return  bed  values
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u.name as userName','i.linkImage', 'i.projectId', 'pa.architectIdArchitect','p.id', 'p.name as projectName', 'p.projectDescription', "REPLACE(i.linkImage,'/static/projects/','/static/projects/mini/' ) as mini")
            ->from('App\Entity\ProjectImages', 'i')
            ->leftJoin(
                'App\Entity\Project', 'p',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'p.id  = i.projectId ')
            ->leftJoin( 'App\Entity\ProjectsArchitects','pa',
            Join::WITH,
                'pa.projetIdProjet = i.projectId ')
            ->leftJoin('App\Entity\User','u',
            Join::WITH,
                'u.id=pa.architectIdArchitect')
            ->orderBy('p.id', 'DESC');

        return $qb->getQuery()->getResult();
    }

}
