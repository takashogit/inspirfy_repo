<?php

namespace App\Repository;

use App\Entity\ProjectsArchitects;
use App\Entity\ProjectImages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProjectsArchitects|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProjectsArchitects|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProjectsArchitects[]    findAll()
 * @method ProjectsArchitects[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectsArchitectsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProjectsArchitects::class);
    }


    public function selectArchitectorWithProjects()
    {
        $em = $this->_em->createQuery("
        SELECT 
    p.architectIdArchitect,
    p.projetIdProjet,
    u.name as userName,
    u.avatar,
    concat('/static/projects/mini/',pr.poster)as miniPoster,
    concat('/static/projects/',pr.poster)as poster,
    pr.name 
    FROM
    App\Entity\ProjectsArchitects  p
        LEFT JOIN
    App\Entity\User u 
    with u.id = p.architectIdArchitect
        LEFT JOIN
    App\Entity\Project pr 
    with p.projetIdProjet = pr.id order by p.architectIdArchitect DESC");
        return $em->getResult();


    }

    public function countImagesInProject( int $projectId)
    {

        $em = $this->_em->createQuery('
        SELECT 
	count(i.linkImage) as imagesInProject 
    FROM
    App\Entity\ProjectImages i
    where i.projectId= :project')
        ->setParameter('project', $projectId);
        return $em->getOneOrNullResult();

    }



    // /**
    //  * @return ProjectsArchitects[] Returns an array of ProjectsArchitects objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProjectsArchitects
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
