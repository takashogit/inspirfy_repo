<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    /**
     * OrderRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }


    public function getuserOrder($user_id)
    {
       // if($user_id >= 0){
        return $this->createQueryBuilder('o')
            ->andWhere('o.user_id = :val')
            ->setParameter('val', $user_id)
            ->getQuery()
            ->getResult();
      //  }else {return false;}
    }



    public function selectUserOrder($userId)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT p.hashProduct , p.title, o.user_id, o.price, o.create_time,o.order_num,
             o.quantity, o.order_status ,i.linkImage  
            FROM  App\Entity\Order o
            LEFT JOIN  App\Entity\Products p  
            WITH  p.hashProduct  = o.hash_product
            LEFT JOIN  App\Entity\Productimg i  
             WITH  i.hashProduct = o.hash_product
            where o.user_id = :user 
            ORDER BY o.create_time DESC')
            ->setParameter('user',$userId);

        return $query->getResult();

    }



    public function getOrderHesh($userid)
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery(
            'SELECT o.hash_product, o.price, o.quantity,o.create_time
            FROM App\Entity\Order o
            LEFT JOIN 
            WHERE o.user_id = :user'
        )->setParameter('user', $userid);
        return $query->getResult();


    }


    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
