<?php


    namespace App\Model;


    class Furniture
    {

        protected $name;
        protected $salePrice;
        protected $image;
        protected $quantityInStock;
        protected $description;
        protected $id;

        /**
         * Furniture constructor.
         * @param $name
         * @param $salePrice
         * @param $image
         * @param $quantityInStock
         * @param $description
         * @param $id
         */
        public function __construct($name, $salePrice, $image, $quantityInStock, $description, $id)
        {
            $this->name = $name;
            $this->salePrice = $salePrice;
            $this->image = $image;
            $this->quantityInStock = $quantityInStock;
            $this->description = $description;
            $this->id = $id;
        }

        /**
         * @return mixed
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         * @return mixed
         */
        public function getSalePrice()
        {
            return $this->salePrice;
        }

        /**
         * @return mixed
         */
        public function getImage()
        {
            return $this->image;
        }

        /**
         * @return mixed
         */
        public function getQuantityInStock()
        {
            return $this->quantityInStock;
        }

        /**
         * @return mixed
         */
        public function getDescription()
        {
            return $this->description;
        }

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }


    }