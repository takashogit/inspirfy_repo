<?php


namespace App\Listeners;


use Exception as ExceptionAlias;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Class AuthenticationSuccessListener
 * @package App\Listeners
 */
class AuthenticationSuccessListener
{


    /*
     * in prod make to true for HTTPS
     */
    private bool $secure = false;
    private $tokenTtl;


    /**
     * AuthenticationSuccessListener constructor.
     * @param $tokenTtl
     */
    public function __construct($tokenTtl)
    {
        $this->tokenTtl = $tokenTtl;
    }


    /**
     * @param AuthenticationSuccessEvent $event
     * @throws ExceptionAlias
     */
    public function onAuthenticationSuccess(AuthenticationSuccessEvent $event)
    {
        $response = $event->getResponse();
        $data = $event->getData();

        $token = $data['token'];
        // unset($data['token']);
        $event->setData($data);


        $response->headers->setCookie(
            new Cookie('BEARER',
                $token,
                (new \DateTime())->add(new \DateInterval('PT' . $this->tokenTtl . 'S'))),
            '/',
            null,
            $this->secure);


    }

}