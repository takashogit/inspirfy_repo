#!/bin/sh
jwt_passhrase=$(grep ''^JWT_PASSPHRASE='' .env | cut -f 2 -d ''='')
mkdir config/jwt
echo "$jwt_passhrase" | openssl genpkey -out config/jwt/private.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
echo "$jwt_passhrase" | openssl pkey -in config/jwt/private.pem -passin stdin -out config/jwt/public.pem -pubout
#setfacl -R -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
#setfacl -dR -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
